package cz.muni.fi.pv217.household.hibernate.orm.panache;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.Response;
import io.smallrye.jwt.build.Jwt;
import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.text.IsEmptyString.emptyString;

@QuarkusTest
public class HouseholdsEndpointTest {

    public static String generateMockToken(UUID userId) {
        return Jwt.issuer("https://example.com/issuer")
                        .groups("member")
                        .subject(userId.toString())
                        .sign();
    }

    @Test
    public void createEntitySucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .body("{\"name\" : \"Cottage\", \"address\": \"Address of new cottage\"}")
                .contentType("application/json")
                .post("/households/")
                .then()
                .statusCode(201)
                .body(
                        containsString("\"id\":"),
                        containsString("\"name\":\"Cottage\""),
                        containsString("\"address\":\"Address of new cottage\""));  // Assert the address is updated

        Response response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/households")
                .then()
                .statusCode(200)
                .contentType("application/json")
                .extract().response();
        assertThat(response.jsonPath().getList("name")).contains("Cottage");
    }

    @Test
    public void createEntityFailsWithIncorrectBodyTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .body("{\"address\": \"Address of new cottage\"}")
                .contentType("application/json")
                .post("/households")
                .then()
                .statusCode(400);
    }


    @Test
    public void getEntitySucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(200)
                .body(
                        containsString("\"id\":"),
                        containsString("\"name\":\"Deda\""),
                        containsString("\"address\":\"Kotlarska 2, 602 00 Brno\""));
    }

    @Test
    public void listEntitiesSucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        Response response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/households")
                .then()
                .statusCode(200)
                .contentType("application/json")
                .extract().response();
        assertThat(response.jsonPath().getList("name")).containsExactlyInAnyOrder("Deda");

        String userId2 = "123e4567-e89b-12d3-a456-426614174000";
        String token2 = generateMockToken(UUID.fromString(userId2));

        Response response2 = given()
                .header("Authorization", "Bearer " + token2)
                .when()
                .get("/households")
                .then()
                .statusCode(200)
                .contentType("application/json")
                .extract().response();
        assertThat(response2.jsonPath().getList("name")).containsExactlyInAnyOrder("Deda",
                "Studentsky byt", "Gaming doupe", "Household to change owner");

        String userId3 = "123e4567-e89b-12d3-a456-426614174003";
        String token3 = generateMockToken(UUID.fromString(userId3));

        Response response3 = given()
                .header("Authorization", "Bearer " + token3)
                .when()
                .get("/households")
                .then()
                .statusCode(200)
                .contentType("application/json")
                .extract().response();
        assertThat(response3.jsonPath().getList("name")).containsExactlyInAnyOrder("Studentsky byt");
    }

    @Test
    public void listEntitiesFailsNotFoundTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174005";
        String token = generateMockToken(UUID.fromString(userId));

        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get(userId + "/households")
                .then()
                .statusCode(404)
                .body(emptyString());
    }

    @Test
    public void updateEntityNotFoundFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174005";
        String token = generateMockToken(UUID.fromString(userId));

        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .body("{\"name\" : \"Spot pod mostem\", \"address\" : \"Karluv most\"}")
                .contentType("application/json")
                .put("/households/32432")
                .then()
                .statusCode(404)
                .body(emptyString());
    }

    @Test
    public void updateEntitySucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .body("{\"name\" : \"Renamed spot pod mostem\", \"address\" : \"Branicky most\"}")
                .contentType("application/json")
                .put("/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(200);

        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .get("/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(200)
                .body(
                        containsString("\"name\":\"Renamed spot pod mostem\""),
                        containsString("\"address\":\"Branicky most\""));  // Assert the address is updated

        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .body("{\"name\" : \"Deda\", \"address\" : \"Kotlarska 2, 602 00 Brno\"}")
                .contentType("application/json")
                .put("/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(200);
    }

    @Test
    public void updateEntityAccountNumberSucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .body("{\"accountNumber\" : \"19-123457/0711\"}")
                .contentType("application/json")
                .put("/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(200);

        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(200)
                .body(
                        containsString("\"name\":\"Deda\""),
                        containsString("\"address\":\"Kotlarska 2, 602 00 Brno\""),
                        containsString("\"accountNumber\":\"19-123457/0711\""));
    }

    @Test
    public void updateOwnerTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        UUID newOwner = UUID.fromString("123e4567-e89b-12d3-a456-426614174009");

        // Update the owner
        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .contentType("application/json")
                .body("{\"ownerId\" : \"123e4567-e89b-12d3-a456-426614174009\"}")
                .put("/households/8c4c854e-d2c5-4fcc-b2fd-52e0f0c5099d/owner")
                .then()
                .log().all()  // Log the response
                .statusCode(200);

        String userId2 = "123e4567-e89b-12d3-a456-426614174009";
        String token2 = generateMockToken(UUID.fromString(userId2));

        // Check if the new owner is updated
        given()
                .header("Authorization", "Bearer " + token2)
                .log().all()  // Log the request
                .when()
                .get("/households/8c4c854e-d2c5-4fcc-b2fd-52e0f0c5099d")
                .then()
                .statusCode(200)
                .body(containsString(newOwner.toString()));

        // Check if the old owner still has access
        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .get("/households/8c4c854e-d2c5-4fcc-b2fd-52e0f0c5099d")
                .then()
                .statusCode(200);

        // Check if the old user has no owner access
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/households/8c4c854e-d2c5-4fcc-b2fd-52e0f0c5099d")
                .then()
                .statusCode(404);
    }

    @Test
    public void updateMembersSucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        Set<UUID> newMemberIds = Set.of(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"), UUID.fromString("123e4567-e89b-12d3-a456-426614174002"),
                UUID.fromString("123e4567-e89b-12d3-a456-426614174007"));

        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .body(newMemberIds)
                .contentType("application/json")
                .put("/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e/members")
                .then()
                .statusCode(200);

        given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .get("/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(200)
                .body(containsString("123e4567-e89b-12d3-a456-426614174000"))
                .body(containsString("123e4567-e89b-12d3-a456-426614174002"))
                .body(containsString("123e4567-e89b-12d3-a456-426614174007"));
    }

    @Test
    public void deleteHouseholdAsOwnerSucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        given()
                .header("Authorization", "Bearer " + token)
                .delete("/households/8c4c854e-d2c5-4fcc-b2fd-52e0f0c5068e")
                .then()
                .statusCode(204);
    }

    @Test
    public void deleteHouseholdAsMemberFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));
        //Delete Deda:
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("123e4567-e89b-12d3-a456-426614174000/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteHouseholdAsNonMemberFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));
        //Delete Deda:
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("123e4567-e89b-12d3-a456-426614174003/households/a7d1b134-bd0e-47f1-8841-bfef65a86a6e")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteEntityNotFoundFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/households/9236")
                .then()
                .statusCode(404)
                .body(emptyString());
    }
}