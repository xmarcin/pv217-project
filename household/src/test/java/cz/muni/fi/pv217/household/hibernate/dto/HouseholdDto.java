package cz.muni.fi.pv217.household.hibernate.orm.panache.entity;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import java.util.Set;
import java.util.UUID;

@Entity
public class HouseholdDto {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    public UUID id;

    @NotBlank
    public String name;

    public String address;

    @ElementCollection(fetch = FetchType.EAGER)
//    @Size(min=1)
    public Set<UUID> memberIds;
}

