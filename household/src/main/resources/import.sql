INSERT INTO Household(id, name, address, accountNumber, ownerId)
VALUES ('a7d1b134-bd0e-47f1-8841-bfef65a86a6e', 'Deda', 'Kotlarska 2, 602 00 Brno', '19-123457/0710', '123e4567-e89b-12d3-a456-426614174002');

INSERT INTO Household(id, name, address, accountNumber, ownerId)
VALUES ('95041a48-11fe-44f0-8dbf-e441c27c4c76', 'Studentsky byt', 'Botanicka 554/68a, 602 00 Brno', '19-123457/0710', '123e4567-e89b-12d3-a456-426614174000');

INSERT INTO Household(id, name, address, accountNumber, ownerId)
VALUES ('8c4c854e-d2c5-4fcc-b2fd-52e0f0c5068e', 'Gaming doupe', 'Arne Novaka 1, 602 00 Brno', '19-123457/0710', '123e4567-e89b-12d3-a456-426614174000');

INSERT INTO Household(id, name, address, accountNumber, ownerId)
VALUES ('8c4c854e-d2c5-4fcc-b2fd-52e0f0c5099d', 'Household to change owner', '', '', '123e4567-e89b-12d3-a456-426614174000');


INSERT INTO Household_memberIds (Household_id, memberIds)
VALUES ('a7d1b134-bd0e-47f1-8841-bfef65a86a6e', '123e4567-e89b-12d3-a456-426614174000');
INSERT INTO Household_memberIds (Household_id, memberIds)
VALUES ('a7d1b134-bd0e-47f1-8841-bfef65a86a6e', '123e4567-e89b-12d3-a456-426614174002');
INSERT INTO Household_memberIds (Household_id, memberIds)
VALUES ('95041a48-11fe-44f0-8dbf-e441c27c4c76', '123e4567-e89b-12d3-a456-426614174000');
INSERT INTO Household_memberIds (Household_id, memberIds)
VALUES ('8c4c854e-d2c5-4fcc-b2fd-52e0f0c5068e', '123e4567-e89b-12d3-a456-426614174000');
INSERT INTO Household_memberIds (Household_id, memberIds)
VALUES ('95041a48-11fe-44f0-8dbf-e441c27c4c76', '123e4567-e89b-12d3-a456-426614174003');
INSERT INTO Household_memberIds (Household_id, memberIds)
VALUES ('8c4c854e-d2c5-4fcc-b2fd-52e0f0c5099d', '123e4567-e89b-12d3-a456-426614174000');

--- 123e4567-e89b-12d3-a456-426614174006