package cz.muni.fi.pv217.household.dto;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.util.Set;
import java.util.UUID;

/**
 * Household data object.
 */
public class HouseholdDto {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    public UUID id;

    public String name;

    public String accountNumber;

    public String address;

    public UUID ownerId;

    public Set<UUID> memberIds;
}

