package cz.muni.fi.pv217.household.hibernate.orm.panache.entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Household Entity represents a household for users.
 */
@Entity
public class Household extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    public UUID id;

    @NotBlank
    public String name;

    public String address;

    public String accountNumber;

    @NotNull
    public UUID ownerId;

    @ElementCollection(fetch = FetchType.EAGER)
    @Size(min = 1)
    public Set<UUID> memberIds = new HashSet<>(Collections.singletonList(ownerId));

    public Household() {

    }

    /**
     * Household constructor with all parameters.
     */
    public Household(UUID id, String name, String address, String accountNumber, UUID ownerId, Set<UUID> memberIds) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.accountNumber = accountNumber;
        this.ownerId = ownerId;
        this.memberIds = memberIds;
    }

    /**
     * Sets new owner of a household and adds its UUID to the list of the members.
     */
    public void setOwner(UUID ownerId) {
        this.ownerId = ownerId;
        addMemberId(ownerId);
    }

    public UUID getOwnerId() {
        return this.ownerId;
    }

    public Set<UUID> getMemberIds() {
        return this.memberIds;
    }

    public void addMemberIds(Set<UUID> memberId) {
        this.memberIds.addAll(memberId);
    }

    public void addMemberId(UUID memberId) {
        this.memberIds.add(memberId);
    }

    public void setMemberIds(Set<UUID> newMemberIds) {
        this.memberIds = newMemberIds;
    }
}

