package cz.muni.fi.pv217.household.hibernate.orm.panache.entity;


import static jakarta.ws.rs.core.Response.Status.CREATED;
import static jakarta.ws.rs.core.Response.Status.NOT_FOUND;

import cz.muni.fi.pv217.household.dto.HouseholdDto;
import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.security.Authenticated;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.jboss.logging.Logger;

/**
 * HouseholdResource provides endpoints for the Household service.
 */

@Path("/households")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
@Authenticated
public class HouseholdResource {
    @Inject
    JsonWebToken jwt;

    private static final Logger LOGGER = Logger.getLogger(HouseholdResource.class.getName());

    private boolean isNotBlank(String str) {
        return str != null && !str.trim().isEmpty();
    }

    /**
     * GET request to obtain all the households for the given member id.
     */
    @GET
    public Uni<Response> get() {
        String userId = jwt.getSubject();
        if (userId == null || userId.isEmpty()) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return Household.list("SELECT h FROM Household h JOIN h.memberIds m WHERE m = ?1", UUID.fromString(userId))
                .onItem().transformToUni(households -> {
                    if (households.isEmpty()) {
                        // Handle the case of an empty list, e.g., return a specific response
                        return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                    } else {
                        // If the list is not empty, return the list wrapped in a response
                        return Uni.createFrom().item(Response.ok(households).build());
                    }
                });
    }

    /**
     * GET request to obtain information about the household for a user.
     *
     * @param id id of the household
     */
    @GET
    @Path("{id}")
    public Uni<Response> getSingle(@PathParam("id") UUID id) {
        String userId = jwt.getSubject();
        if (userId == null || userId.isEmpty()) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return Household.find("FROM Household h WHERE h.id = ?1 AND ?2 MEMBER OF h.memberIds",
                        id, UUID.fromString(userId))
                .firstResult()
                .onItem().transformToUni(household -> {
                    if (household != null) {
                        // If the household is found, return it in the response
                        return Uni.createFrom().item(Response.ok(household).build());
                    } else {
                        // If the household is not found, return a 404 Not Found response
                        return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                    }
                });
    }


    /**
     * POST request to create a new household record.
     *
     * @param householdDto household object holding all the new information to create the household
     */
    @POST
    public Uni<Response> create(@Valid HouseholdDto householdDto) {
        String userId = jwt.getSubject();
        if (userId == null || userId.isEmpty()) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        if (Objects.equals(householdDto.name, null)) {
            return Uni.createFrom().item(Response.status(Response.Status.BAD_REQUEST).build());
        }
        UUID user = UUID.fromString(userId);
        Set<UUID> members = new HashSet<>(Collections.singletonList(user));
        Household household = new Household(householdDto.id, householdDto.name, householdDto.address,
                householdDto.accountNumber, user, members);
        return Panache.withTransaction(household::persist)
                .replaceWith(Response.ok(household).status(CREATED)::build);
    }

    /**
     * PUT request to update the information about household: name, address, and account number.
     *
     * @param id           household to update
     * @param householdDto household object holding all the new information to update
     */
    @PUT
    @Path("{id}")
    public Uni<Response> update(UUID id, @Valid HouseholdDto householdDto) {
        String userId = jwt.getSubject();
        if (userId == null || userId.isEmpty()) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }
        LOGGER.infof("%s", userId);


        Uni<Household> h = Household.find("id = ?1 AND ownerId = ?2", id, UUID.fromString(userId)).firstResult();
        return h
                .onItem().ifNotNull().transformToUni(household -> {
                    return Panache.withTransaction(() -> {
                        if (isNotBlank(householdDto.name)) {
                            household.name = householdDto.name;
                        }
                        if (isNotBlank(householdDto.address)) {
                            household.address = householdDto.address;
                        }
                        if (isNotBlank(householdDto.accountNumber)) {
                            household.accountNumber = householdDto.accountNumber;
                        }

                        return Uni.createFrom().item(household);
                    });
                })
                .onItem().ifNotNull().transform(updatedHousehold -> {
                    return Response.ok(updatedHousehold).build();
                })
                .onItem().ifNull().continueWith(() -> {
                    // Handle the case where the household is not found or the user is not the owner
                    LOGGER.info("Household not found or not owned by user");
                    return Response.status(NOT_FOUND).build();
                });
    }

    /**
     * DELETE request to delete the whole household.
     *
     * @param id household to delete
     */
    @DELETE
    @Path("{id}")
    public Uni<Response> delete(UUID id) {
        String userId = jwt.getSubject();
        if (userId == null || userId.isEmpty()) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        LOGGER.infof("Going to delete an object with uuid %s", id);
        return Household.find("id = ?1 AND ownerId = ?2", id, UUID.fromString(userId)).firstResult()
                .onItem().transformToUni(household -> {
                    if (household != null) {
                        // Proceed with deletion
                        return Panache.withTransaction(() -> Household.deleteById(id))
                                .map(deleted -> deleted
                                        ? Response.ok().status(Response.Status.NO_CONTENT).build()
                                        : Response.ok().status(Response.Status.NOT_FOUND).build());
                    } else {
                        // Not allowed to delete or household not found
                        return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                    }
                });
    }

    /**
     * PUT request to update the list of members of the given household.
     *
     * @param id           household to update
     * @param newMemberIds list of new UUIDs of members
     */
    @PUT
    @Path("{id}/members")
    public Uni<Response> updateMembers(@PathParam("id") UUID id, Set<UUID> newMemberIds) {
        String userId = jwt.getSubject();
        if (userId == null || userId.isEmpty()) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return Household.<Household>find("id = ?1 AND ownerId = ?2", id, UUID.fromString(userId))
                .firstResult()
                .onItem().transformToUni(household -> {
                    if (household == null) {
                        return Uni.createFrom().item(Response.status(NOT_FOUND).build());
                    }
                    if (!household.getOwnerId().equals(UUID.fromString(userId))) {
                        return Uni.createFrom().item(Response.status(Response.Status.FORBIDDEN).build());
                    }
                    household.setMemberIds(newMemberIds);
                    return Panache.withTransaction(household::persist)
                            .replaceWith(Response.ok(household).build());
                });
    }

    /**
     * PUT request to update the owner of the household.
     * This method can be used only by the owner to give up the ownership on behalf of different user.
     * After doing so, the user stays on the member list of the household but no longer has
     * the rights to update or delete the household.
     *
     * @param id           household to update
     * @param householdDto household object with the id of new owner
     */
    @PUT
    @Path("{id}/owner")
    public Uni<Response> updateOwner(UUID id, @Valid HouseholdDto householdDto) {
        String userId = jwt.getSubject();
        if (userId == null || userId.isEmpty()) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        Uni<Household> h = Household.find("id = ?1 AND ownerId = ?2", id, UUID.fromString(userId)).firstResult();
        return h
                .onItem().ifNotNull().transformToUni(household -> {
                    return Panache.withTransaction(() -> {
                        LOGGER.info(householdDto.ownerId);
                        if (householdDto.ownerId != null) {
                            household.ownerId = householdDto.ownerId;
                            household.memberIds.add(householdDto.ownerId);
                        }

                        return Uni.createFrom().item(household);
                    });
                })
                .onItem().ifNotNull().transform(updatedHousehold -> {
                    return Response.ok(updatedHousehold).build();
                })
                .onItem().ifNull().continueWith(() -> {
                    // Handle the case where the household is not found or the user is not the owner
                    LOGGER.info("Household not found or not owned by user");
                    return Response.status(NOT_FOUND).build();
                });
    }
}
