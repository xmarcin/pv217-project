# Household Management

Platform for managing common household
- track utilities consumption
- plan and distribute chores
- share common expenses

## Why is Microservice appropriate
- There are several functions that we can separate into unique services. The maintenance of services allows more accessible adaptations based on user needs.
- Scaling: Adding new households or users does not have to affect the whole system, as it is easier to scale only one of the services.
- Resilience: If one of the services crashes (for example, the parts that take care of the chore distribution), the rest can work independently.

## Benefits of Using Microservices in This Project
- Different households can have different needs. That said, adding more services for some entities tracking and not using certain services is more effortless.
- Managing some features in one service without impacting other services is easier.

## Drawbacks of Microservices in This Project
- For a smaller scale, as if we had only one user with one household, setting up the microservice does not have to be worth it.
- Using authentication (i.e. ownership) has to be distributed over all of the services, which could cause initial difficulties. As a result, redundant effort in the implementation of the authentication might occur.

## Microservices Architecture

![System Diagram](./blueprints/structurizr-SystemLandscape-001.png)

Other diagrams can be found in the [blueprints](./blueprints/) directory.

## Deployment

The microservices can be deployed with Podman/Docker compose using the included container-compose.yml file.
For production deployment, it is strongly preferred to copy secret environment variable to `.env` files and including them in the compose.
**Each microservice/db pairs needs its own `.env` file, due to clash in config names.**

You also need to generate a RSA key pair for signing/verifying JSON Web Tokens.
You can generate those with this script.

```sh
# Optionally, you can set these environment variables to change the location where keys are saved.
# PUBLIC_KEY=publicKey.pem PRIVATE_KEY=privateKey.pem ./scripts/deploy/gen-jwt-key.sh
./scripts/deploy/gen-jwt-key.sh
```

Then you need to build container images of microservices (or pull them from the quay.io repository, however, the first option is preferred).
There is no need to build the projects first, as the custom Containerfile includes a separate build stage which builds the microservices from source.

```sh
# Feel free to substitute podman-compose with your preferred compose command
podman-compose build
```

Finally, you can start all services with this command:

```sh
podman-compose up -d
```

The included compose includes these published services:

| Service               | Ports | OpenAPI/UI link                       | Default Login |
|-----------------------|-------|---------------------------------------|---------------|
| Users Management      | 8080  | http://localhost:8080/apidocs         |               |
| Household Management  | 8081  | http://localhost:8081/q/swagger-ui    |               |
| Utilities Tracker     | 8082  | http://localhost:8082/q/swagger-ui    |               |
| Chores Planner        | 8083  | http://localhost:8083/q/swagger-ui    |               |
| Common Expenses       | 8084  | http://localhost:8084/q/swagger-ui    |               |
| Prometheus            | 9090  | http://localhost:9090/                |               |
| Grafana               | 3000  | http://localhost:3000/                | admin:admin   |

Running services can be also inspected using

```sh
podman-compose ps
```

All microservices report their health status to the container runtime and are automatically restarted in case they are stopped unexpectedly.
If you want to automatically restart the container if it reports unhealthy status, replace the healthcheck command in the compose file with:


```diff
-      test: ["CMD", "/deployments/quarkus-ready.sh"]
+      test: ["CMD", "/deployments/quarkus-live.sh", "kill"]
```

The `quarkus-live.sh kill` healthcheck kills the running container if it is unhealthy and the container is then restared by the container runtime.

## Demo Scenario

The project contains Locust script that showcases the functions of the system under load.
To run this scenario you need to have Locust installed:

```sh
# Optional, but preferred: run in virtual environment:
python -m venv .
pip install -r ./scripts/scenario/requirements.txt

locust -f ./scripts/scenario/locustfile.py
```

Locust UI is then available at http://localhost:8089 by default.

## User Requirements

### User Management

- User should be able to create account with e-mail
- User should be able to update their account information
- User should be able to set their bank account number
- User should be able to delete all ther information 
- User should able to login and receive JWT token

### Household Management

- User should be able to able to create a household (becomes its Owner and also Member)
- Owner should be able to add other users to their household (added user becomes its Member)
- Owner should be able to remove members from their household
- Owner should be able to update information of their household
- Owner should be able to delete their household
- Owner should be able to transfer household ownership to another household Member (Member becomes the only Owner)

### Utilities Tracker

- Owner should be able to manage utility meters
- Member should be able to add utility meter measurements
- Owner should be able to delete utility meter measurements
- Member should be able to view utility meter measurements in paged table

### Chores Planner

- Owner should be able to manage chores, their frequency and recurrence and distribution among Members
- Member should be able to add completed chore record
- Owner should be able to delete completed chore record
- Member should be able to view completed chore records in a table

### Common Expenses

- Owner should be able to define required recurring contributions from each Member and their recurrence
- Member should be able to add expenses
- Member should be to specify individual share of the expense among members (percentage or absolute value)
- Member should be able to revert expenses
- Member should be able to view total household balance
- Member should be able to view theirs personal household balance
- Owner should be able to view personal household balance for each member
