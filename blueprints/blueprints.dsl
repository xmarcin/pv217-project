workspace {

  model {
    user = person "User" "A user of the Household Management System."
    admin = person "Administrators" "System administrators responsible for monitoring system metrics."

    userManagementSystem = softwareSystem "User Management System" "Manages user information and authentication." {
      userApi = container "User API" "Provides user authentication, management capabilities, and token generation." "REST API, Java/Spring Boot" {
        authenticationController = component "Authentication Controller" "Handles user authentication requests. Utilizes DTOs and mappers for data transformation."
        userController = component "User Controller" "Manages user information. Employs DTOs for data transfer between layers."
      }
      userDatabase = container "User Database" "Stores user credentials and profiles." "SQL Database"
    }

    householdManagementSystem = softwareSystem "Household Management System" "Manages household creation, member management, and household settings." {
      householdApi = container "Household API" "Handles household and member management operations." "REST API, Node.js/Express" {
        householdController = component "Household Controller" "Manages household-related operations. Uses DTOs for data representation."
      }
      householdDatabase = container "Household Database" "Stores household and member information." "NoSQL Database"
    }

    choresManagementSystem = softwareSystem "Chores Management System" "Enables chore assignment, tracking, and management within households." {
      choresApi = container "Chores API" "Manages chores assignment and tracking." "REST API, Python/Flask" {
        choresController = component "Chores Controller" "Handles chores management. Utilizes DTOs for organizing and transferring chore data."
      }
      choresDatabase = container "Chores Database" "Stores chores data." "SQL Database"
    }

    expensesManagementSystem = softwareSystem "Expenses Management System" "Facilitates management of household expenses, member contributions, and balance tracking." {
      expensesApi = container "Expenses API" "Manages household expenses and contributions." "REST API, Ruby on Rails" {
        expensesController = component "Expenses Controller" "Handles expenses operations. Uses DTOs for financial data handling."
      }
      expensesDatabase = container "Expenses Database" "Stores expenses data." "SQL Database"
    }

    utilitiesManagementSystem = softwareSystem "Utilities Management System" "Manages utility data, meter readings, and owner/member interactions with utilities." {
      utilitiesApi = container "Utilities API" "Handles utility data and meter readings." "REST API, Go" {
        utilitiesController = component "Utilities Controller" "Manages utility data interactions. Employs DTOs for data structuring."
      }
      utilitiesDatabase = container "Utilities Database" "Stores utility data." "NoSQL Database"
    }
    prometheus = softwareSystem "Prometheus" "Monitoring tool that collects and stores metrics."
    grafana = softwareSystem "Grafana" "Analytics and monitoring platform that visualizes metrics."

    prometheus -> userManagementSystem "Fetches metrics from"
    prometheus -> householdManagementSystem "Fetches metrics from"
    prometheus -> choresManagementSystem "Fetches metrics from"
    prometheus -> expensesManagementSystem "Fetches metrics from"
    prometheus -> utilitiesManagementSystem "Fetches metrics from"

    grafana -> prometheus "Gets metrics from"
    admin -> grafana "Views dashboards on"

    user -> userApi "Registers/Logs in"
    userApi -> userDatabase "Reads from/Writes to"
    user -> householdApi "Uses"
    householdApi -> userApi "Validates token with"
    householdApi -> householdDatabase "Reads from/Writes to"
    user -> choresApi "Uses"
    choresApi -> choresDatabase "Reads from/Writes to"
    user -> expensesApi "Uses"
    expensesApi -> expensesDatabase "Reads from/Writes to"
    user -> utilitiesApi "Uses"
    utilitiesApi -> utilitiesDatabase "Reads from/Writes to"
    
    householdApi -> userApi "Validates user sessions using"
    choresApi -> householdApi "Gets household details from"
    expensesApi -> householdApi "Verifies household membership using"
    utilitiesApi -> householdApi "Checks household utility settings using"

  }

  // Define views
  views {
    systemLandscape {
      include *
      autoLayout lr
    }

    systemContext userManagementSystem {
      include *
      autoLayout lr
    }

    container userManagementSystem {
      include *
      autoLayout lr
    }

    systemContext householdManagementSystem {
      include *
      autoLayout lr
    }

    container householdManagementSystem {
      include *
      autoLayout lr
    }

    systemContext choresManagementSystem {
      include *
      autoLayout lr
    }

    container choresManagementSystem {
      include *
      autoLayout lr
    }

    systemContext expensesManagementSystem {
      include *
      autoLayout lr
    }

    container expensesManagementSystem {
      include *
      autoLayout lr
    }

    systemContext utilitiesManagementSystem {
      include *
      autoLayout lr
    }

    container utilitiesManagementSystem {
      include *
      autoLayout lr
    }
  }
}


