package cz.muni.fi.pv217.chores.security.jwt;

import io.smallrye.jwt.build.Jwt;
import java.util.UUID;

/**
 * Generate JWT token.
 */
public class GenerateToken {
    /**
     * Generating of token for testing purpose.
     */
    public static void main(String[] args) {
        String token =
                Jwt.issuer("https://example.com/issuer")
                        .groups("member")
                        .subject("123e4567-e89b-12d3-a456-426614174000")
                        .sign();
        System.out.println(token);
    }

    /**
     * Generates a new token for a member.
     *
     * @param memberId household to update.
     */
    public static void generateMemberToken(UUID memberId) {
        String token =
                Jwt.issuer("https://example.com/issuer")
                        .subject(memberId.toString())
                        .signWithSecret("BofosadyX9gNX-4eWrrdQCCzFpifP7x5riKBr67DSso=");
        System.out.println(token);
    }
}
