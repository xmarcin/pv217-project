package cz.muni.fi.pv217.chores.hibernate.orm.panache.entity;


import static cz.muni.fi.pv217.chores.hibernate.orm.panache.entity.common.Common.isBlank;
import static jakarta.ws.rs.core.Response.Status.CREATED;
import static jakarta.ws.rs.core.Response.Status.NOT_FOUND;

import cz.muni.fi.pv217.chores.dto.ChoreAssignmentDto;
import cz.muni.fi.pv217.chores.hibernate.orm.panache.entity.common.HouseholdApi;
import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.security.Authenticated;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.rest.client.inject.RestClient;

/**
 * HouseholdResource provides endpoints for the Household service.
 */

@Path("/chores-assignment")
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
@Authenticated
public class ChoreAssignmentResource {
    @Inject
    @Claim(standard = Claims.sub)
    String userId;

    @RestClient
    HouseholdApi householdApi;

    private static final Logger LOGGER = Logger.getLogger(ChoreAssignmentResource.class.getName());

    /**
     * GET request to get a chore.
     */
    @GET
    @Path("{id}")
    public Uni<Response> getSingle(UUID id) {
        if (isBlank(userId)) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return Panache.withTransaction(() -> ChoreAssignment.<ChoreAssignment>findById(id)
                .onItem()
                .ifNotNull()
                .transformToUni(
                        assignment -> Chore.<Chore>findById(assignment.choreId).onItem().transformToUni(chore -> {
                            String householdId = String.valueOf(chore.householdId);
                            return householdApi.getHousehold(householdId)
                                    .onItem()
                                    .ifNotNull()
                                    .transformToUni(householdDto -> {
                                        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
                                        String memberId = String.valueOf(assignment.memberId);
                                        if (!isOwner && !Objects.equals(memberId, userId)) {
                                            LOGGER.info(String.valueOf(assignment.memberId));
                                            LOGGER.info(userId);

                                            return Uni.createFrom()
                                                    .item(Response.status(Response.Status.NOT_FOUND).build());
                                        }
                                        return Panache.withTransaction(() -> Uni.createFrom().item(assignment))
                                                .onItem()
                                                .transform(entity -> Response.ok(assignment).build());
                                    });
                        }))).onItem().ifNull().continueWith(Response.ok().status(NOT_FOUND)::build);
    }

    /**
     * A helper method to get householdId by choreId.
     */
    public Uni<UUID> getHouseholdIdByChoreId(UUID choreId) {
        return Chore.<Chore>findById(choreId)
                .onItem()
                .ifNotNull()
                .transform(chore -> chore.householdId)
                .onItem()
                .ifNull()
                .failWith(() -> new NotFoundException("Chore not found"));
    }

    /**
     * POST request to assign chore to a member.
     */
    @POST
    @Path("/assign")
    public Uni<Response> assignChore(@Valid ChoreAssignmentDto assignmentDto) {
        return getHouseholdIdByChoreId(assignmentDto.getChoreId()).onItem()
                .transformToUni(householdId -> householdApi.getHousehold(String.valueOf(householdId))
                        .onItem()
                        .ifNotNull()
                        .transformToUni(householdDto -> {

                            boolean isOwner = Objects.equals(householdDto.ownerId, userId);
                            if (isBlank(userId) || !isOwner) {
                                return Uni.createFrom().item(Response.status(Response.Status.FORBIDDEN).build());
                            }

                            ChoreAssignment newAssignment = new ChoreAssignment();
                            newAssignment.choreId = assignmentDto.getChoreId();
                            newAssignment.memberId = assignmentDto.getMemberId();
                            newAssignment.startDate = assignmentDto.getStartDate();
                            newAssignment.completedDate = null;
                            return Panache.withTransaction(newAssignment::persist)
                                    .onItem()
                                    .transform(ignored -> Response.ok(ignored).status(CREATED).build());
                        })
                        .onItem()
                        .ifNull()
                        .continueWith(Response.ok().status(NOT_FOUND)::build));
    }

    private Uni<ChoreAssignment> checkIsOwnerOrMember(ChoreAssignment assignment, String userId) {
        return Chore.<Chore>findById(assignment.choreId).onItem().transformToUni(chore -> {
            String householdId = String.valueOf(chore.householdId);
            return householdApi.getHousehold(householdId).onItem().ifNotNull().transformToUni(householdDto -> {
                boolean hasAccess = householdDto.ownerId.equals(userId) || householdDto.memberIds.contains(userId);
                return hasAccess ? Uni.createFrom().item(assignment) : Uni.createFrom().nullItem();
            });
        });
    }

    /**
     * GET request to get a list of completed chores.
     */
    @GET
    @Path("/household/{id}/completed")
    public Uni<Response> getCompleted(UUID id) {
        if (isBlank(userId)) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return Panache.withTransaction(() -> ChoreAssignment.<ChoreAssignment>list(
                        "SELECT ca FROM ChoreAssignment ca JOIN ca.chore c "
                                + "WHERE c.householdId = ?1 AND ca.completedDate IS NOT NULL", id)
                .onItem()
                .ifNotNull()
                .transformToUni(assignments -> {
                    // Fetch the Chore entity using the choreId from ChoreAssignment
                    if (assignments.isEmpty()) {
                        return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                    } else {
                        // Filter the assignments based on user's access to the household
                        return Uni.combine()
                                .all()
                                .unis(assignments.stream()
                                        .map(assignment -> checkIsOwnerOrMember(assignment, userId))
                                        .collect(Collectors.toList()))
                                .combinedWith(accessibleAssignments -> accessibleAssignments.stream()
                                        .filter(Objects::nonNull)
                                        .collect(Collectors.toList()))
                                .onItem()
                                .transform(filteredAssignments -> Response.ok(filteredAssignments).build());
                    }
                })).onItem().ifNull().continueWith(Response.ok().status(NOT_FOUND)::build);
    }

    /**
     * PUT request to update the information about chore.
     */
    @PUT
    @Path("{id}/completed")
    public Uni<Response> update(UUID id) {
        if (isBlank(userId)) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return Panache.withTransaction(
                () -> ChoreAssignment.<ChoreAssignment>findById(id).onItem().ifNotNull().transformToUni(assignment ->
                        // Fetch the Chore entity using the choreId from ChoreAssignment
                        Chore.<Chore>findById(assignment.choreId).onItem().transformToUni(chore -> {
                            // Now you have access to householdId from the Chore entity
                            String householdId = String.valueOf(chore.householdId);
                            return householdApi.getHousehold(householdId)
                                    .onItem()
                                    .ifNotNull()
                                    .transformToUni(householdDto -> {
                                        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
                                        boolean isMember = householdDto.memberIds.contains(userId);
                                        if (!isMember && !isOwner) {
                                            return Uni.createFrom()
                                                    .item(Response.status(Response.Status.NOT_FOUND).build());
                                        }

                                        if (!Objects.equals(String.valueOf(assignment.memberId), userId) && !isOwner) {
                                            return Uni.createFrom()
                                                    .item(Response.status(Response.Status.UNAUTHORIZED).build());
                                        }
                                        assignment.completedDate = LocalDate.now();
                                        return Panache.withTransaction(() -> Uni.createFrom().item(assignment))
                                                .onItem()
                                                .transform(updatedEntity -> Response.ok(updatedEntity).build());
                                    });
                        }))).onItem().ifNull().continueWith(Response.ok().status(NOT_FOUND)::build);
    }

    /**
     * DELETE request to delete the chore.
     *
     * @param id household to delete
     */
    @DELETE
    @Path("{id}")
    public Uni<Response> delete(UUID id) {
        if (isBlank(userId)) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return ChoreAssignment.<ChoreAssignment>findById(id).onItem().transformToUni(assignment -> {
            if (assignment == null) {
                return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
            }
            return Chore.<Chore>findById(assignment.choreId).onItem().ifNotNull().transformToUni(chore -> {
                String householdId = String.valueOf(chore.householdId);
                return householdApi.getHousehold(householdId).onItem().ifNotNull().transformToUni(householdDto -> {
                    boolean isOwner = Objects.equals(householdDto.ownerId, userId);
                    if (isOwner && assignment.completedDate != null) {
                        return Panache.withTransaction(() -> ChoreAssignment.deleteById(id))
                                .map(deleted -> deleted ? Response.ok().status(Response.Status.NO_CONTENT).build()
                                        : Response.ok().status(Response.Status.NOT_FOUND).build());
                    }
                    if (!isOwner) {
                        return Uni.createFrom().item(Response.status(Response.Status.FORBIDDEN).build());
                    }
                    return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                });
            }).onItem().ifNull().continueWith(Response.ok().status(NOT_FOUND)::build);
        }).onItem().ifNull().continueWith(Response.ok().status(NOT_FOUND)::build);
    }
}


