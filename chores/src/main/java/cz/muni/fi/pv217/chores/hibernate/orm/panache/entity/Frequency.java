package cz.muni.fi.pv217.chores.hibernate.orm.panache.entity;

/**
 * Frequency of given chore.
 */
public enum Frequency {
    DAILY,
    WEEKLY,
    MONTHLY
}
