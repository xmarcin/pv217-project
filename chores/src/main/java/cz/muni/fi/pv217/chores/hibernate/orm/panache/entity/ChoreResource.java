package cz.muni.fi.pv217.chores.hibernate.orm.panache.entity;


import static cz.muni.fi.pv217.chores.hibernate.orm.panache.entity.common.Common.isBlank;
import static jakarta.ws.rs.core.Response.Status.CREATED;
import static jakarta.ws.rs.core.Response.Status.NOT_FOUND;

import cz.muni.fi.pv217.chores.dto.ChoreDto;
import cz.muni.fi.pv217.chores.hibernate.orm.panache.entity.common.HouseholdApi;
import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.security.Authenticated;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Logger;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.rest.client.inject.RestClient;

/**
 * HouseholdResource provides endpoints for the Household service.
 */

@Path("/chores")
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
@Authenticated
public class ChoreResource {
    @Inject
    @Claim(standard = Claims.sub)
    String userId;

    @RestClient
    HouseholdApi householdApi;
    private static final Logger LOGGER = Logger.getLogger(ChoreResource.class.getName());

    /**
     * GET request to get a list of chores.
     */
    @GET
    @Path("/household/{id}")
    public Uni<Response> get(UUID id) {
        return householdApi.getHousehold(String.valueOf(id)).onItem().ifNotNull().transformToUni(householdDto -> {
            boolean isOwner = Objects.equals(householdDto.ownerId, userId);
            if (isBlank(userId) || !isOwner) {
                return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
            }

            return Chore.list("householdId = ?1 AND deleted=false", id).onItem().transformToUni(ch -> {
                if (ch.isEmpty()) {
                    // Handle the case of an empty list, e.g., return a specific response
                    return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                } else {
                    // If the list is not empty, return the list wrapped in a response
                    return Uni.createFrom().item(Response.ok(ch).build());
                }
            });
        }).onItem().ifNull().continueWith(Response.ok().status(NOT_FOUND)::build);
    }

    /**
     * GET request to get a chore.
     */
    @GET
    @Path("{id}")
    public Uni<Response> getSingle(UUID id) {
        if (isBlank(userId)) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return Chore.<Chore>find("id = ?1 and deleted = false", id)
                .firstResult()
                .onItem()
                .ifNotNull()
                .transformToUni(chore -> householdApi.getHousehold(String.valueOf(chore.householdId))
                        .onItem()
                        .ifNotNull()
                        .transformToUni(householdDto -> {
                            boolean isOwner = Objects.equals(householdDto.ownerId, userId);
                            if (chore != null && isOwner) {
                                // If the household is found, return it in the response
                                return Uni.createFrom().item(Response.ok(chore).build());
                            } else {
                                // If the household is not found, return a 404 Not Found response
                                return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                            }
                        })
                        .onItem()
                        .ifNull()
                        .continueWith(Response.ok().status(NOT_FOUND)::build))
                .onItem()
                .ifNull()
                .continueWith(Response.ok().status(NOT_FOUND)::build);
    }

    /**
     * POST request to create a new chore type.
     */
    @POST
    public Uni<Response> create(@Valid Chore chore) {
        return householdApi.getHousehold(String.valueOf(chore.householdId))
                .onItem()
                .ifNotNull()
                .transformToUni(householdDto -> {
                    boolean isOwner = Objects.equals(householdDto.ownerId, userId);
                    if (isBlank(userId) || !isOwner) {
                        return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                    }

                    return Panache.withTransaction(chore::persist)
                            .replaceWith(Response.ok(chore).status(CREATED)::build);
                })
                .onItem()
                .ifNull()
                .continueWith(Response.ok().status(NOT_FOUND)::build);
    }

    /**
     * PUT request to update the information about chore.
     */
    @PUT
    @Path("{id}")
    public Uni<Response> update(UUID id, ChoreDto chore) {
        if (isBlank(userId) || chore == null) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        LOGGER.info(userId);

        return Panache.withTransaction(() -> Chore.<Chore>find("id = ?1 and deleted = false", id)
                .firstResult()
                .onItem()
                .ifNotNull()
                .transformToUni(entity -> {
                    if (entity == null) {
                        // Chore not found
                        return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                    }

                    return householdApi.getHousehold(String.valueOf(entity.householdId))
                            .onItem()
                            .ifNotNull()
                            .transformToUni(householdDto -> {
                                boolean isOwner = Objects.equals(householdDto.ownerId, userId);
                                if (!isOwner) {
                                    return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                                }
                                if (!isBlank(chore.name)) {
                                    entity.name = chore.name;
                                }
                                if (!isBlank(chore.description)) {
                                    entity.description = chore.description;
                                }

                                if (!isBlank(String.valueOf(chore.frequency))) {
                                    entity.frequency = chore.frequency;
                                }

                                if (chore.recurrence != null) {
                                    entity.recurrence = chore.recurrence;
                                }

                                if (chore.durationMinutes != null) {
                                    entity.durationMinutes = chore.durationMinutes;
                                }

                                if (chore.deleted) {
                                    entity.deleted = chore.deleted;
                                }
                                return Panache.withTransaction(() -> Uni.createFrom().item(entity))
                                        .onItem()
                                        .transform(updatedEntity -> Response.ok(updatedEntity).build());
                            })
                            .onItem()
                            .ifNull()
                            .continueWith(Response.ok().status(NOT_FOUND)::build);
                }));
    }

    /**
     * DELETE request to delete the chore.
     *
     * @param id household to delete
     */
    @DELETE
    @Path("{id}")
    public Uni<Response> delete(UUID id) {
        if (isBlank(userId)) {
            return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
        }

        return Panache.withTransaction(() -> Chore.<Chore>find("id = ?1 and deleted = false", id)
                .firstResult()
                .onItem()
                .ifNotNull()
                .transformToUni(entity -> householdApi.getHousehold(String.valueOf(entity.householdId))
                        .onItem()
                        .ifNotNull()
                        .transformToUni(householdDto -> {
                            boolean isOwner = Objects.equals(householdDto.ownerId, userId);
                            if (!isOwner) {
                                return Uni.createFrom().item(Response.status(Response.Status.NOT_FOUND).build());
                            }
                            entity.deleted = true;
                            return Uni.createFrom().item(Response.ok(entity).build());
                        })
                        .onItem()
                        .ifNull()
                        .continueWith(Response.ok().status(NOT_FOUND)::build))
                .onItem()
                .ifNull()
                .continueWith(Response.ok().status(NOT_FOUND)::build));
    }
}


