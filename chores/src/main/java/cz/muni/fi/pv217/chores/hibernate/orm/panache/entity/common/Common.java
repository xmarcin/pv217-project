package cz.muni.fi.pv217.chores.hibernate.orm.panache.entity.common;

import java.util.UUID;
import org.eclipse.microprofile.rest.client.inject.RestClient;

/**
 * Shared functions by both Chore and ChoreAssignment.
 */

public class Common {
    @RestClient
    HouseholdApi householdApi;

    /**
     * Validates if the given user is an owner of a household.
     */
    public static boolean isOwner(UUID householdId, UUID userId) {

        return true;
    }

    /**
     * Validates if the given user is a member of a household.
     */
    public static boolean isMember(UUID householdId, UUID userId) {
        return true;
    }

    /**
     * Validates if string field is not empty.
     */
    public static boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }
}
