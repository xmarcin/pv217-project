package cz.muni.fi.pv217.chores.hibernate.orm.panache.entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

/**
 * Chore Assignment represents a chore assigned to a household member.
 */
@Entity
public class ChoreAssignment extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    public UUID id;

    @ManyToOne
    @JoinColumn(name = "choreId", referencedColumnName = "id", insertable = false, updatable = false)
    private Chore chore;

    @NotNull
    public UUID choreId;

    @NotNull
    public UUID memberId;

    @NotNull
    public LocalDate startDate;

    public LocalDate completedDate;

    /**
     * ChoreAssignment constructor with all parameters.
     */
    public ChoreAssignment() {
    }
}

