package cz.muni.fi.pv217.chores.hibernate.orm.panache.entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.UUID;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

/**
 * Chore entity represents a single chore and its specification.
 */
@Entity
public class Chore extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    public UUID id;

    @NotNull
    public UUID householdId;

    @NotBlank
    public String name;

    public String description;

    /**
     * Estimates a frequency of given chore.
     */
    public Frequency frequency;

    /**
     * Specifies how many times the chore has to be done for the given frequency.
     */
    public Integer recurrence;

    /**
     * Specifies the estimate amount of duration of chore in minutes.
     */

    public Integer durationMinutes;

    @Column(name = "deleted", nullable = false)
    public Boolean deleted = false;

    /**
     * Chore constructor with all parameters.
     */
    public Chore(UUID id, String name, String description, Frequency frequency, int recurrence, int durationMinutes) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.frequency = frequency;
        this.recurrence = recurrence;
        this.durationMinutes = durationMinutes;
    }

    public Chore() {

    }

    public UUID getHouseholdId() {
        return this.householdId;
    }

    /**
     * Estimates monthly minutes spent by a chore.
     */
    public int choreMonthlyEstimation() {
        int doneMonthly = 1;
        switch (this.frequency) {
            case DAILY:
                doneMonthly = 30;
                break;
            case WEEKLY:
                doneMonthly = 4;
                break;
            case MONTHLY:
            default:
                break;
        }
        return doneMonthly * this.durationMinutes * this.recurrence;
    }
}

