package cz.muni.fi.pv217.chores.dto;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

/**
 * Chore Assignment DTO represents a chore assigned to a household member.
 */
@Entity
public class ChoreAssignmentDto extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    public UUID id;

    public UUID choreId;

    public UUID memberId;

    public LocalDate startDate;

    public LocalDate completedDate;

    public UUID getId() {
        return id;
    }

    public UUID getChoreId() {
        return choreId;
    }

    public UUID getMemberId() {
        return memberId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getCompletedDate() {
        return completedDate;
    }

}
