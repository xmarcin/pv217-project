package cz.muni.fi.pv217.chores.dto;

import cz.muni.fi.pv217.chores.hibernate.orm.panache.entity.Frequency;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.util.UUID;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

/**
 * Chore DTO represents a single chore and its specification.
 */
@Entity
@SQLDelete(sql = "UPDATE chore SET deleted = TRUE WHERE id = ?", check = ResultCheckStyle.COUNT)
public class ChoreDto extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    public UUID id;

    public UUID householdId;

    public String name;

    public String description;

    public Frequency frequency;

    public Integer recurrence;

    public Integer durationMinutes;

    public boolean deleted = false;
}

