--- Owner of a7d1b134-bd0e-47f1-8841-bfef65a86a6e is 123e4567-e89b-12d3-a456-426614174002
INSERT INTO Chore(id, householdId, name, description, frequency, recurrence, durationMinutes, deleted)
VALUES ('82fde383-e213-4a84-94ea-6bf5616a6328', 'a7d1b134-bd0e-47f1-8841-bfef65a86a6e', 'Sweep', '', 1, 1, 20, false);

INSERT INTO Chore(id, householdId, name, description, frequency, recurrence, durationMinutes, deleted)
VALUES ('7e53e0a3-c78d-4e82-af23-6c0004ab87fa', 'a7d1b134-bd0e-47f1-8841-bfef65a86a6e',  'Vacuum', '', 1, 1, 20, false);

INSERT INTO Chore(id, householdId, name, description, frequency, recurrence, durationMinutes, deleted)
VALUES ('855e0437-bc5a-47eb-b176-62630a9d6f86', 'a7d1b134-bd0e-47f1-8841-bfef65a86a6e',
        'Clean windows', 'clean shared windows', 2, 1, 20, false);

INSERT INTO Chore(id, householdId, name, description, frequency, recurrence, durationMinutes, deleted)
VALUES ('bca96ede-e266-4f2a-ba32-9ea327ca1439', 'a7d1b134-bd0e-47f1-8841-bfef65a86a6e',
        'Clean the kitchen', 'take care of the stove, the kitchen unit, and the table', 1, 3, 5, false);

INSERT INTO Chore(id, householdId, name, description, frequency, recurrence, durationMinutes, deleted)
VALUES ('908f6f2d-7c84-4eea-bf4a-42f3362b2f34', 'a7d1b134-bd0e-47f1-8841-bfef65a86a6e',
        'Clean the bathroom', 'wash the shower, sink and the toilet', 1, 1, 30, false);

INSERT INTO Chore(id, householdId, name, description, frequency, recurrence, durationMinutes, deleted)
VALUES ('17ccd4e8-1314-42ea-b572-25bceded7939', 'a7d1b134-bd0e-47f1-8841-bfef65a86a6e',
        'Empty bins', 'empty mixed waste and recycling bins', 1, 2, 5, false);


-- --- Weekly short tasks
INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('754a335c-c4dd-4596-943d-9f87a782b0a5', '82fde383-e213-4a84-94ea-6bf5616a6328',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-05');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('43375bc7-8b27-4b2b-950a-f602d71592b0', '7e53e0a3-c78d-4e82-af23-6c0004ab87fa',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-05');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('9e7c1f96-c483-4b65-b00b-85c07b480599', '82fde383-e213-4a84-94ea-6bf5616a6328',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-12');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('b98e130b-4a3e-46b7-ad8d-821febcbd90c', '7e53e0a3-c78d-4e82-af23-6c0004ab87fa',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-12');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('63ede4b5-780c-4a13-91ec-70b66f413642', '82fde383-e213-4a84-94ea-6bf5616a6328',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-19');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('1e7c956a-ab35-4280-b513-709673750010', '7e53e0a3-c78d-4e82-af23-6c0004ab87fa',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-19');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('d63b8f59-5894-4282-8ba7-c837c6bd12e7', '82fde383-e213-4a84-94ea-6bf5616a6328',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-26');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('3a79f51a-2f18-41bb-953e-6b535c88e382', '7e53e0a3-c78d-4e82-af23-6c0004ab87fa',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-26');


---MONTHLY clean shared windows
INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('3fae4ddc-9749-4edb-86a2-2132613c5332', '855e0437-bc5a-47eb-b176-62630a9d6f86',
        '123e4567-e89b-12d3-a456-426614174000', '2023-12-30');


--- Clean the kitchen, WEEKLY 5 min
INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('22418e0f-e370-44e8-bea2-7490aaa0812b', 'bca96ede-e266-4f2a-ba32-9ea327ca1439',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-05');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('ae9d1018-3fdb-4c2f-8e72-aa10dfb12a7f', 'bca96ede-e266-4f2a-ba32-9ea327ca1439',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-12');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('4cb12f44-8766-46ce-a928-27e3b95972db', 'bca96ede-e266-4f2a-ba32-9ea327ca1439',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-19');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('b1b9c3bd-b41b-4141-94f1-3ff205a83016', 'bca96ede-e266-4f2a-ba32-9ea327ca1439',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-26');


--- Wash the shower, WEEKLY 5 min
INSERT INTO ChoreAssignment(id, choreId, memberId, startDate, completedDate)
VALUES ('44200296-00a9-4377-921d-52da8b91cd78', '908f6f2d-7c84-4eea-bf4a-42f3362b2f34',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-05', '2023-11-06');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate, completedDate)
VALUES ('8d29c456-b182-47a5-8eb7-bbc52ec013e4', '908f6f2d-7c84-4eea-bf4a-42f3362b2f34',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-12', '2023-11-12');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('4e4ea6ff-d28f-49d6-926d-78ceeb187936', '908f6f2d-7c84-4eea-bf4a-42f3362b2f34',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-19');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('b1a0d724-78ca-4fde-b29a-2dd57815bffd', '908f6f2d-7c84-4eea-bf4a-42f3362b2f34',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-26');

--- Empty the bins
INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('aab13958-a900-4dd8-b447-d6b8bbe94fd4', '17ccd4e8-1314-42ea-b572-25bceded7939',
        '123e4567-e89b-12d3-a456-426614174005', '2023-11-05');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('8ccedd90-46fb-4359-af84-508ad3528992', '17ccd4e8-1314-42ea-b572-25bceded7939',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-12');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate)
VALUES ('6db10bce-f9d6-459f-bd24-4edc1708d20f', '17ccd4e8-1314-42ea-b572-25bceded7939',
        '123e4567-e89b-12d3-a456-426614174002', '2023-11-19');

INSERT INTO ChoreAssignment(id, choreId, memberId, startDate, completedDate)
VALUES ('17ccd4e8-1314-42ea-b572-25bceded7937', '17ccd4e8-1314-42ea-b572-25bceded7939',
        '123e4567-e89b-12d3-a456-426614174000', '2023-11-26', '2023-11-30');
