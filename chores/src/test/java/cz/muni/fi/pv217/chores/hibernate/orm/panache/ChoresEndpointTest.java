package cz.muni.fi.pv217.chores.hibernate.orm.panache;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyString;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.smallrye.jwt.build.Jwt;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class ChoresEndpointTest extends TestsBase {
    public static String generateMockToken(UUID userId) {
        return Jwt.issuer("https://example.com/issuer")
                .groups("member")
                .subject(userId.toString())
                .sign();
    }

    @Test
    public void createEntitySucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .body("{\"householdId\" : \"a7d1b134-bd0e-47f1-8841-bfef65a86a6e\"," +
                        "\"name\": \"name of the new chore\"," +
                        "\"description\": \"description\"," +
                        "\"frequency\": \"WEEKLY\"," +
                        "\"recurrence\": \"1\"," +
                        "\"durationMinutes\": \"1\" }")
                .contentType("application/json")
                .post("/chores/")
                .then()
                .statusCode(201)
                .body(
                        containsString("\"id\":"),
                        containsString("\"name\":\"name of the new chore\""),
                        containsString("\"description\":\"description\""));  // Assert the address is updated

        Response response = RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .pathParam("id", testHousehold.id)
                .get("/chores/household/{id}")
                .then()
                .statusCode(200)
                .contentType("application/json")
                .extract().response();
        assertThat(response.jsonPath().getList("name")).contains("name of the new chore");
    }

    @Test
    public void createEntityFailsWithIncorrectBodyTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .body("{\"address\": \"Address of new cottage\"}")
                .contentType("application/json")
                .post("/chores")
                .then()
                .statusCode(400);
    }


    @Test
    public void getEntitySucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores/7e53e0a3-c78d-4e82-af23-6c0004ab87fa")
                .then()
                .statusCode(200)
                .body(
                        containsString("\"id\":"),
                        containsString("\"name\":\"Vacuum\""));
    }

    @Test
    public void listEntitiesSucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        Response response = RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .pathParam("id", testHousehold.id)
                .get("/chores/household/{id}")
                .then()
                .statusCode(200)
                .contentType("application/json")
                .extract().response();
        assertThat(response.jsonPath().getList("name")).containsExactlyInAnyOrder("Sweep",
                "Vacuum",
                "Clean windows",
                "Clean the kitchen",
                "Clean the bathroom",
                "Empty bins");
    }

    @Test
    public void listEntitiesFailsNotFoundTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174005";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores/123232")
                .then()
                .statusCode(404)
                .body(emptyString());
    }

    @Test
    public void updateEntityAsNonOwnerFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174005";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .log().all()  // Log the request
                .when()
                .body("{\"name\" : \"Updating entity should fail\"}")
                .contentType("application/json")
                .put("/chores/82fde383-e213-4a84-94ea-6bf5616a6328")
                .then()
                .statusCode(404)
                .body(emptyString());
    }

    @Test
    public void updateEntitySucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .body("{\"name\" : \"Renamed vacuum entity\"}")
                .contentType("application/json")
                .put("/chores/7e53e0a3-c78d-4e82-af23-6c0004ab87fa")
                .then()
                .statusCode(200);

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores/7e53e0a3-c78d-4e82-af23-6c0004ab87fa")
                .then()
                .statusCode(200)
                .body(
                        containsString("\"name\":\"Renamed vacuum entity\""));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .body("{\"name\" : \"Vacuum\"}")
                .contentType("application/json")
                .put("/chores/7e53e0a3-c78d-4e82-af23-6c0004ab87fa")
                .then()
                .statusCode(200);
    }

    @Test
    public void deleteHouseholdAsOwnerSucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores/17ccd4e8-1314-42ea-b572-25bceded7939")
                .then()
                .statusCode(200);

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .delete("/chores/17ccd4e8-1314-42ea-b572-25bceded7939")
                .then()
                .statusCode(200);

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores/17ccd4e8-1314-42ea-b572-25bceded7939")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteChoreAsMemberFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/chores/82fde383-e213-4a84-94ea-6bf5616a6000")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteChoreAsNonMemberFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/chores/82fde383-e213-4a84-94ea-6bf5616a6328")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteEntityNotFoundFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/chores/9236")
                .then()
                .statusCode(404)
                .body(emptyString());
    }
}


