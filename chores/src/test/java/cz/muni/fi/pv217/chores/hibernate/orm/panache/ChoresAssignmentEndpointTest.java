package cz.muni.fi.pv217.chores.hibernate.orm.panache;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.smallrye.jwt.build.Jwt;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class ChoresAssignmentEndpointTest extends TestsBase {
    public static String generateMockToken(UUID userId) {
        return Jwt.issuer("https://example.com/issuer")
                .groups("member")
                .subject(userId.toString())
                .sign();
    }

    @Test
    public void getAssignedChoreAsHouseholdMember() {
        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType("application/json")
                .get("/chores-assignment/754a335c-c4dd-4596-943d-9f87a782b0a5")
                .then()
                .statusCode(200)
                .body(
                        containsString("\"startDate\":\"2023-11-05\""));
    }

    @Test
    public void assignChoreToAMemberAsOwnerSuccceedsTest() {
        String ownerId = "123e4567-e89b-12d3-a456-426614174002";
        String tokenOwner = generateMockToken(UUID.fromString(ownerId));

        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        String assignmentId = RestAssured.given()
                .header("Authorization", "Bearer " + tokenOwner)
                .when()
                .body("{\"choreId\" : \"908f6f2d-7c84-4eea-bf4a-42f3362b2f34\"," +
                        "\"memberId\" : \"123e4567-e89b-12d3-a456-426614174000\"," +
                        "\"startDate\" : \"2023-12-11\"}")
                .contentType("application/json")
                .post("/chores-assignment/assign")
                .then()
                .statusCode(201)
                .extract().body().path("id");

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .log().all()
                .when()
                .get("/chores-assignment/" + assignmentId)
                .then()
                .statusCode(200)
                .body("completedDate", nullValue());
    }

    @Test
    public void assignChoreToAMemberAsMemberFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174005";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores-assignment/123232")
                .then()
                .statusCode(404)
                .body(emptyString());
    }

    @Test
    public void listEntitiesFailsNotFoundTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174005";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores-assignment/123232")
                .then()
                .statusCode(404)
                .body(emptyString());
    }

    @Test
    public void listCompletedAsHouseholdMember() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType("application/json")
                .pathParam("id", testHousehold.id)
                .get("/chores-assignment/household/{id}/completed")
                .then()
                .statusCode(200)
                .body(
                        containsString("\"id\":\"8d29c456-b182-47a5-8eb7-bbc52ec013e4\""));
    }

    @Test
    public void markAsCompletedAsOwnerSucceedsTest() {
        String ownerId = "123e4567-e89b-12d3-a456-426614174002";
        String ownerToken = generateMockToken(UUID.fromString(ownerId));

        RestAssured.given()
                .header("Authorization", "Bearer " + ownerToken)
                .when()
                .get("/chores-assignment/4e4ea6ff-d28f-49d6-926d-78ceeb187936")
                .then()
                .statusCode(200)
                .body("completedDate", nullValue());

        RestAssured.given()
                .header("Authorization", "Bearer " + ownerToken)
                .when()
                .contentType("application/json")
                .put("/chores-assignment/4e4ea6ff-d28f-49d6-926d-78ceeb187936/completed")
                .then()
                .statusCode(200);

        RestAssured.given()
                .header("Authorization", "Bearer " + ownerToken)
                .log().all()
                .when()
                .get("/chores-assignment/4e4ea6ff-d28f-49d6-926d-78ceeb187936")
                .then()
                .statusCode(200)
                .body("completedDate", notNullValue());
    }

    @Test
    public void markAsCompletedAsAssignedMemberSucceedsTest() {
        String ownerId = "123e4567-e89b-12d3-a456-426614174002";
        String ownerToken = generateMockToken(UUID.fromString(ownerId));

        String userId = "123e4567-e89b-12d3-a456-426614174000";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + ownerToken)
                .when()
                .get("/chores-assignment/8ccedd90-46fb-4359-af84-508ad3528992")
                .then()
                .statusCode(200)
                .body("completedDate", nullValue());

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType("application/json")
                .put("/chores-assignment/8ccedd90-46fb-4359-af84-508ad3528992/completed")
                .then()
                .statusCode(200);

        RestAssured.given()
                .header("Authorization", "Bearer " + ownerToken)
                .log().all()
                .when()
                .get("/chores-assignment/8ccedd90-46fb-4359-af84-508ad3528992")
                .then()
                .statusCode(200)
                .body("completedDate", notNullValue());
    }

    @Test
    public void markAsCompletedAsAssignedButNotMemberFailsTest() {
        String ownerId = "123e4567-e89b-12d3-a456-426614174002";
        String ownerToken = generateMockToken(UUID.fromString(ownerId));

        String userId = "123e4567-e89b-12d3-a456-426614174005";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + ownerToken)
                .when()
                .get("/chores-assignment/aab13958-a900-4dd8-b447-d6b8bbe94fd4")
                .then()
                .statusCode(200)
                .body("completedDate", nullValue());

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType("application/json")
                .put("/chores-assignment/aab13958-a900-4dd8-b447-d6b8bbe94fd4/completed")
                .then()
                .statusCode(404);

        RestAssured.given()
                .header("Authorization", "Bearer " + ownerToken)
                .log().all()
                .when()
                .get("/chores-assignment/aab13958-a900-4dd8-b447-d6b8bbe94fd4")
                .then()
                .statusCode(200)
                .body("completedDate", nullValue());
    }

    @Test
    public void deleteChoreAssignmentAsOwnerCompletedSucceedsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores-assignment/44200296-00a9-4377-921d-52da8b91cd78")
                .then()
                .statusCode(200);

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/chores-assignment/44200296-00a9-4377-921d-52da8b91cd78")
                .then()
                .statusCode(204);

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/chores-assignment/44200296-00a9-4377-921d-52da8b91cd78")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteChoreAssignmentAsOwnerNotCompletedFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/chores-assignment/9e7c1f96-c483-4b65-b00b-85c07b480599")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteChoreAssignmentMemberFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));

        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/chores-assignment/123e4567-e89b-12d3-a456-426614174000")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteChoresAssignmentNotFoundFailsTest() {
        String userId = "123e4567-e89b-12d3-a456-426614174002";
        String token = generateMockToken(UUID.fromString(userId));
        RestAssured.given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/chores-assignment/9236")
                .then()
                .statusCode(404)
                .body(emptyString());
    }
}