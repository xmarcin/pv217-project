package cz.muni.fi.pv217.chores.hibernate.orm.panache;

import static org.mockito.Mockito.when;

import com.github.javafaker.Faker;
import cz.muni.fi.pv217.chores.hibernate.orm.panache.entity.common.HouseholdApi;
import cz.muni.fi.pv217.chores.hibernate.orm.panache.entity.common.HouseholdDto;
import io.quarkus.test.InjectMock;
import io.smallrye.mutiny.Uni;
import java.util.Set;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeEach;

public abstract class TestsBase {
    @InjectMock
    @RestClient
    HouseholdApi householdApi;

    public HouseholdDto testHousehold = TestsBase.fakeHouseholdDto();

    private static HouseholdDto fakeHouseholdDto() {
        Faker faker = new Faker();
        HouseholdDto householdDto = new HouseholdDto();
        householdDto.id = "a7d1b134-bd0e-47f1-8841-bfef65a86a6e";
        householdDto.name = faker.commerce().productName();
        householdDto.ownerId = "123e4567-e89b-12d3-a456-426614174002";
        householdDto.memberIds = Set.of("123e4567-e89b-12d3-a456-426614174000", "123e4567-e89b-12d3-a456-426614174002");
        householdDto.address = faker.address().fullAddress();
        householdDto.accountNumber = faker.finance().iban();
        return householdDto;
    }

    @BeforeEach
    public void setupMockApi() {
        when(householdApi.getHousehold(testHousehold.id)).thenReturn(Uni.createFrom().item(testHousehold));
    }
}
