package cz.muni.fi.pv217.chores.hibernate.orm.panache;

import io.quarkus.test.junit.QuarkusIntegrationTest;

@QuarkusIntegrationTest
class ChoresEndpointIT extends ChoresEndpointTest {
    // Execute the same tests but in packaged mode.
}
