package cz.muni.fi.pv217.household.utilities.common.models;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import java.util.List;

/**
 * Class representing paged result.
 */
public class PagedResult<T> {
    public List<T> results;
    public Long count;
    public int pageCount;
    public int pageIndex;
    public int pageSize;

    /**
     * Creates a new PagedResult from query and page info.
     *
     * @param query query to load data from
     * @param page  page data
     */
    public PagedResult(PanacheQuery<T> query, Page page) {
        results = query.list();
        count = query.count();
        pageCount = query.pageCount();
        pageIndex = page.index;
        pageSize = page.size;
    }
}

