package cz.muni.fi.pv217.household.utilities.measurement;

import cz.muni.fi.pv217.household.utilities.measurement.dtos.MeasurementCreateDto;
import cz.muni.fi.pv217.household.utilities.measurement.models.Measurement;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Class handling Meter Measurements.
 */
@RequestScoped
public class MeasurementService {
    /**
     * Lists measurements of a meter.
     *
     * @param meterId meter id
     * @return measurements list
     */

    @Transactional
    public List<Measurement> listMeasurements(long meterId) {
        StringBuilder queryBuilder = new StringBuilder("meterId = :meterId");
        Parameters queryParams = Parameters.with("meterId", meterId);

        queryBuilder.append(" and (deletedAt = NULL )");

        return Measurement.list(queryBuilder.toString(), Measurement.timestampAtDesc(), queryParams);
    }

    private void setMeasurementFields(Measurement measurement, MeasurementCreateDto measurementCreateDto) {
        measurement.value = measurementCreateDto.value;
        measurement.comment = measurementCreateDto.comment;
        measurement.timestamp = measurementCreateDto.timestamp;
    }

    /**
     * Creates a measurement.
     *
     * @param meterId meter Id
     * @param measurementCreateDto measurement to create
     * @return created measurement
     */
    @Transactional
    public Measurement createMeasurement(long meterId, MeasurementCreateDto measurementCreateDto) {
        Measurement measurement = new Measurement();
        measurement.meterId = meterId;
        setMeasurementFields(measurement, measurementCreateDto);
        measurement.persist();
        return measurement;
    }

    /**
     * Finds a measurement.
     *
     * @param meterId       meter id
     * @param measurementId measurement id
     * @return measurement
     */
    private Measurement findMeasurement(long meterId, String measurementId) {
        Measurement measurement = Measurement.findById(measurementId);
        if (measurement == null || measurement.meterId != meterId) {
            throw new NotFoundException("Measurement not found");
        }
        return measurement;
    }

    /**
     * Delete a measurement from a meter.
     *
     * @param meterId       meter Id
     * @param measurementId measurement Id
     * @return deleted measurement
     */

    @Transactional
    public Measurement deleteMeasurement(long meterId, String measurementId) {
        Measurement measurement = findMeasurement(meterId, measurementId);
        measurement.deletedAt = LocalDateTime.now();
        return measurement;
    }
}
