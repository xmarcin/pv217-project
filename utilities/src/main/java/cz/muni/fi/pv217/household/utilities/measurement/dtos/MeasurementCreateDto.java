package cz.muni.fi.pv217.household.utilities.measurement.dtos;

import cz.muni.fi.pv217.household.utilities.common.dtos.BaseDto;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Measurement creating data object.
 */
public class MeasurementCreateDto extends BaseDto {

    @NotNull
    public double value;

    @Nullable
    public String comment;

    @NotNull
    public LocalDateTime timestamp;
}
