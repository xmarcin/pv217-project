package cz.muni.fi.pv217.household.utilities.common;

import java.util.List;

/**
 * Base mapper.
 *
 * @param <DtoT>   DTO class type
 * @param <ModelT> model class type
 */
public interface BaseMapper<DtoT, ModelT> {

    DtoT toResource(ModelT transaction);

    List<DtoT> toResources(List<ModelT> transactions);


}