package cz.muni.fi.pv217.household.utilities.common.dtos;

import java.time.LocalDateTime;

/**
 * Base class for data transfer.
 */

public abstract class BaseDto {
    public long id;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;
}