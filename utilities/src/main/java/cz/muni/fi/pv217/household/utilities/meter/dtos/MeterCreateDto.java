package cz.muni.fi.pv217.household.utilities.meter.dtos;

import cz.muni.fi.pv217.household.utilities.metertype.dtos.MeterType;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;

/**
 * Class representing Meter data object.
 */
public class MeterCreateDto {
    @NotBlank(message = "Name must not be blank.")
    public String name;

    @Nullable
    public String comment;


    public MeterType type;

    @NotBlank
    public String unit;
}