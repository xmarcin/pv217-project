package cz.muni.fi.pv217.household.utilities.measurement;

import cz.muni.fi.pv217.household.utilities.household.HouseholdApi;
import cz.muni.fi.pv217.household.utilities.household.HouseholdDto;
import cz.muni.fi.pv217.household.utilities.measurement.dtos.MeasurementCreateDto;
import cz.muni.fi.pv217.household.utilities.measurement.dtos.MeasurementDto;
import io.quarkus.security.Authenticated;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;

/**
 * Endpoint for meter measurements.
 */
@Path("/{householdId}/meters/{meterId}/measurements")
@Authenticated
@RequestScoped
public class MeasurementResource {

    @Inject
    MeasurementMapper measurementMapper;

    @Inject
    MeasurementService measurementService;

    @PathParam("householdId")
    private String householdId;

    @PathParam("meterId")
    private long meterId;

    @Inject
    @Claim(standard = Claims.sub)
    String userId;

    @RestClient
    HouseholdApi householdApi;

    /**
     * Lists meters in a household.
     *
     * @return meters list
     */

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MeasurementDto> index() {
        householdApi.getHousehold(householdId);
        return measurementMapper.toResources(measurementService.listMeasurements(meterId));
    }


    /**
     * Creates a new measurement in ak meter.
     *
     * @param measurementCreateDto meter create data
     * @return created meter
     */
    @POST
    @ResponseStatus(201)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public MeasurementDto create(MeasurementCreateDto measurementCreateDto) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        // TODO: verify that this needs to be here
        if (!householdDto.memberIds.contains(userId)) {
            throw new BadRequestException("User is not part of the household");
        }
        return measurementMapper.toResource((measurementService.createMeasurement(meterId, measurementCreateDto)));
    }

    /**
     * Delete a measurement in a meter. Only for household owner.
     *
     * @param measurementId measurement id
     * @return deleted measurement
     */

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{measurementId}")
    public MeasurementDto delete(@RestPath String measurementId) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        if (!isOwner) {
            throw new BadRequestException("Only owner can delete a meter");
        }
        if (!householdDto.memberIds.contains(userId)) {
            throw new BadRequestException("User is not part of the household");
        }
        return measurementMapper.toResource(measurementService.deleteMeasurement(meterId, measurementId));
    }

}
