package cz.muni.fi.pv217.household.utilities.household;


import io.quarkus.rest.client.reactive.ClientExceptionMapper;
import io.quarkus.security.UnauthorizedException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * Class for handling Household API calls.
 */
@ApplicationScoped
@RegisterRestClient(configKey = "household-api")
@RegisterClientHeaders
@Path("/households")
public interface HouseholdApi {

    @GET
    @Path("/{householdId}")
    HouseholdDto getHousehold(@PathParam("householdId") String householdId);

    /**
     * Maps client response to an exception.
     *
     * @param response client response
     * @return exception or null
     */
    @ClientExceptionMapper
    static RuntimeException toException(Response response) {
        if (response.getStatus() == 404) {
            return new NotFoundException("Household not found");
        }
        if (response.getStatus() == 401) {
            return new UnauthorizedException();
        }
        return null;
    }
}
