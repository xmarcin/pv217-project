package cz.muni.fi.pv217.household.utilities.meter;

import cz.muni.fi.pv217.household.utilities.meter.dtos.MeterCreateDto;
import cz.muni.fi.pv217.household.utilities.meter.models.Meter;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Class handling Meters.
 */
@RequestScoped
public class MeterService {
    /**
     * Lists meters in a household.
     *
     * @param householdId household id
     * @return meters list
     */
    public List<Meter> listMeters(String householdId) {
        StringBuilder queryBuilder = new StringBuilder("householdId = :householdId");
        Parameters queryParams = Parameters.with("householdId", householdId);
        queryBuilder.append(" and (deletedAt = NULL )");

        return Meter.list(queryBuilder.toString(), Meter.createdAtDesc(), queryParams);
    }

    private void setMeterFields(Meter meter, MeterCreateDto meterCreateDto) {
        meter.name = meterCreateDto.name;
        meter.comment = meterCreateDto.comment;
        meter.type = meterCreateDto.type;
        meter.unit = meterCreateDto.unit;
    }

    /**
     * Finds a meter.
     *
     * @param householdId household id
     * @param meterId     meter id
     * @return measurement
     */
    public Meter findMeter(String householdId, String meterId) {
        Meter meter = Meter.findById(meterId);
        if (meter == null || !meter.householdId.equals(householdId)) {
            throw new NotFoundException("Meter not found");
        }
        return meter;
    }


    /**
     * Creates a new meter.
     *
     * @param householdId    household id
     * @param meterCreateDto meter create data
     * @return created meter
     */
    @Transactional
    public Meter createMeter(String householdId, MeterCreateDto meterCreateDto) {
        Meter meter = new Meter();
        meter.householdId = householdId;
        setMeterFields(meter, meterCreateDto);
        meter.persist();
        return meter;
    }

    /**
     * Deletes a meter.
     *
     * @param householdId household id
     * @param meterId     meter id
     * @return deleted meter
     */
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Meter deleteMeter(String householdId, String meterId) {
        Meter meter = findMeter(householdId, meterId);
        meter.deletedAt = LocalDateTime.now();
        //        meter.delete();
        return meter;
    }

    @Transactional
    public Meter getMeter(String householdId, String meterId) {
        return findMeter(householdId, meterId);
    }

    /**
     * Updates a Meter.
     *
     * @param householdId    household id
     * @param meterId        meter id to be updated
     * @param meterCreateDto meter create dto with data
     * @return updated meter
     */
    @Transactional
    public Meter updateMeter(String householdId, String meterId, MeterCreateDto meterCreateDto) {
        Meter meter = findMeter(householdId, meterId);
        setMeterFields(meter, meterCreateDto);
        return meter;
    }
}
