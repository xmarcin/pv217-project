package cz.muni.fi.pv217.household.utilities.measurement.models;

import cz.muni.fi.pv217.household.utilities.common.models.BaseEntity;
import jakarta.annotation.Nullable;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Class representing Measurement for a Meter.
 */
@Entity
public class Measurement extends BaseEntity {
    @NotNull
    public long meterId; // TODO: foreign key of something?

    @NotNull
    public double value;

    @Nullable
    public String comment;

    @NotNull
    public LocalDateTime timestamp;

    @Nullable
    public LocalDateTime deletedAt;

}


