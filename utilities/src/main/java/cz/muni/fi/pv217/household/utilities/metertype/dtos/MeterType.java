package cz.muni.fi.pv217.household.utilities.metertype.dtos;

/**
 * Enum for Meter types.
 */
public enum MeterType {
    Electricity, Water, Heating, NaturalGas

}
