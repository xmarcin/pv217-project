package cz.muni.fi.pv217.household.utilities.meter.dtos;

import cz.muni.fi.pv217.household.utilities.common.dtos.BaseDto;
import cz.muni.fi.pv217.household.utilities.metertype.dtos.MeterType;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * Class representing Meter data object.
 */
public class MeterDto extends BaseDto {

    @NotNull
    public String householdId;
    @NotBlank
    public String name;

    @Nullable
    public String comment;


    public MeterType type;

    @NotBlank
    public String unit;

}

