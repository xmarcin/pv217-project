package cz.muni.fi.pv217.household.utilities.common.dtos;

import java.util.List;

/**
 * Class for transfer of paged result.
 *
 * @param <T> item Dto type
 */
public class PagedResultDto<T> {
    public List<T> results;
    public long count;
    public int pageCount;
    public int pageIndex;
    public int pageSize;
}