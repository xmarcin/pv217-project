package cz.muni.fi.pv217.household.utilities.meter.models;

import cz.muni.fi.pv217.household.utilities.common.models.BaseEntity;
import cz.muni.fi.pv217.household.utilities.metertype.dtos.MeterType;
import jakarta.annotation.Nullable;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Class representing Meter in a household.
 */
@Entity
public class Meter extends BaseEntity {

    @NotNull
    public String householdId;
    @NotBlank
    public String name;

    public String comment;

    public MeterType type;

    @NotBlank
    public String unit;

    @Nullable
    public LocalDateTime deletedAt;

}
