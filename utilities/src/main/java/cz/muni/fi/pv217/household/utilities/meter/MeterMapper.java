package cz.muni.fi.pv217.household.utilities.meter;


import cz.muni.fi.pv217.household.utilities.common.BaseMapper;
import cz.muni.fi.pv217.household.utilities.meter.dtos.MeterDto;
import cz.muni.fi.pv217.household.utilities.meter.models.Meter;
import org.mapstruct.Mapper;

/**
 * Class for mapping Meter.
 */
@Mapper(componentModel = "jakarta")
public interface MeterMapper extends BaseMapper<MeterDto, Meter> {
}