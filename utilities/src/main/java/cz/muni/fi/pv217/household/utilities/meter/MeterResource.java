package cz.muni.fi.pv217.household.utilities.meter;

import cz.muni.fi.pv217.household.utilities.household.HouseholdApi;
import cz.muni.fi.pv217.household.utilities.household.HouseholdDto;
import cz.muni.fi.pv217.household.utilities.meter.dtos.MeterCreateDto;
import cz.muni.fi.pv217.household.utilities.meter.dtos.MeterDto;
import io.quarkus.security.Authenticated;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;

/**
 * Class handing Meter REST resource.
 */
@Path("/{householdId}/meters")
@Authenticated
@RequestScoped
public class MeterResource {

    @Inject
    MeterMapper meterMapper;

    @Inject
    MeterService meterService;

    @Inject
    @Claim(standard = Claims.sub)
    String userId;

    @PathParam("householdId")
    private String householdId;

    @RestClient
    HouseholdApi householdApi;

    /**
     * Lists meters in a household.
     *
     * @return meters list
     */

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MeterDto> index() {
        householdApi.getHousehold(householdId);
        return meterMapper.toResources(meterService.listMeters(householdId));
    }

    /**
     * Creates a new meter in a household.
     *
     * @param meterCreateDto meter create data
     * @return created meter
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseStatus(201)
    public MeterDto create(@Valid MeterCreateDto meterCreateDto) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        if (!isOwner) {
            throw new BadRequestException("Only owner can add meter");
        }
        // TODO: verify that this needs to be here
        if (!householdDto.memberIds.contains(userId)) {
            throw new BadRequestException("User is not part of the household");
        }

        return meterMapper.toResource(meterService.createMeter(householdId, meterCreateDto));
    }

    /**
     * Deletes a meter in a household.
     *
     * @param meterId meter id
     * @return deleted meter
     */

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meterId}")
    public MeterDto get(@RestPath String meterId) {
        householdApi.getHousehold(householdId);
        return meterMapper.toResource(meterService.getMeter(householdId, meterId));
    }

    /**
     * Updates a meter in a household.
     *
     * @param meterId meter id
     * @return updated meter
     */

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meterId}")
    public MeterDto update(@RestPath String meterId, @Valid MeterCreateDto meterCreateDto) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        if (!isOwner) {
            throw new BadRequestException("Only owner can update meters");
        }
        // TODO: verify that this needs to be here
        if (!householdDto.memberIds.contains(userId)) {
            throw new BadRequestException("User is not part of the household");
        }

        return meterMapper.toResource(meterService.updateMeter(householdId, meterId, meterCreateDto));
    }

    /**
     * Deletes a meter in a household.
     *
     * @param meterId meter id
     * @return deleted meter
     */


    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meterId}")
    public MeterDto delete(@RestPath String meterId) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        if (!isOwner) {
            throw new BadRequestException("Only owner can delete meters.");
        }
        return meterMapper.toResource(meterService.deleteMeter(householdId, meterId));
    }


}
