package cz.muni.fi.pv217.household.utilities.measurement;

import cz.muni.fi.pv217.household.utilities.common.BaseMapper;
import cz.muni.fi.pv217.household.utilities.measurement.dtos.MeasurementDto;
import cz.muni.fi.pv217.household.utilities.measurement.models.Measurement;
import org.mapstruct.Mapper;

/**
 * Class for mapping measurements.
 */
@Mapper(componentModel = "jakarta")
public interface MeasurementMapper extends BaseMapper<MeasurementDto, Measurement> {
}
