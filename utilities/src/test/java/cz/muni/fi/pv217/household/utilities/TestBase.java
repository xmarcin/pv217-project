package cz.muni.fi.pv217.household.utilities;

import com.github.javafaker.Faker;
import cz.muni.fi.pv217.household.utilities.household.HouseholdApi;
import cz.muni.fi.pv217.household.utilities.household.HouseholdDto;
import io.quarkus.test.InjectMock;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeEach;

import java.util.Set;

import static org.mockito.Mockito.when;

public abstract class TestBase {
    @InjectMock
    @RestClient
    HouseholdApi householdApi;

    public Faker faker = new Faker();

    public HouseholdDto testHousehold = TestBase.fakeHouseholdDto();

    public RequestSpecification given() {
        return RestAssured.given().pathParam("householdId", testHousehold.id);
    }

    private static HouseholdDto fakeHouseholdDto() {
        Faker faker = new Faker();
        HouseholdDto householdDto = new HouseholdDto();
        householdDto.id = faker.internet().uuid();
        householdDto.name = faker.commerce().productName();
        householdDto.ownerId = "user0";
        householdDto.memberIds = Set.of("user0", "user1", "user2", "user3", "user4");
        householdDto.address = faker.address().fullAddress();
        householdDto.accountNumber = faker.finance().iban();
        return householdDto;
    }

    @BeforeEach
    public void setupMockApi() {
        when(householdApi.getHousehold(testHousehold.id)).thenReturn(testHousehold);
    }
}

