package cz.muni.fi.pv217.household.utilities.meter;

import com.github.javafaker.Faker;
import cz.muni.fi.pv217.household.utilities.meter.dtos.MeterCreateDto;
import cz.muni.fi.pv217.household.utilities.meter.models.Meter;
import cz.muni.fi.pv217.household.utilities.metertype.dtos.MeterType;


public class MeterUtils {
    public static Meter fakeMeter(Faker faker, String householdId) {
        Meter meter = new Meter();
        meter.name = faker.commerce().productName();
        meter.householdId = householdId != null ? householdId : faker.internet().uuid();
        //meter.comment =
        //meter.deletedAt =
        meter.type = MeterType.Electricity;
        meter.unit = "kWh";


        return meter;
    }

    public static MeterCreateDto fakeMeterCreateDto(Faker faker) {
        MeterCreateDto meterCreateDto = new MeterCreateDto();
        meterCreateDto.name = faker.commerce().productName();
        meterCreateDto.unit = "kWh";
        meterCreateDto.type = MeterType.Electricity;
//        meterCreateDto.comment =
        return meterCreateDto;
    }
}