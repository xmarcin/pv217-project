package cz.muni.fi.pv217.household.utilities.meter;

import static cz.muni.fi.pv217.household.utilities.meter.MeterUtils.fakeMeter;
import static cz.muni.fi.pv217.household.utilities.meter.MeterUtils.fakeMeterCreateDto;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.not;

import cz.muni.fi.pv217.household.utilities.TestBase;
import cz.muni.fi.pv217.household.utilities.meter.dtos.MeterCreateDto;
import cz.muni.fi.pv217.household.utilities.meter.models.Meter;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.ClaimType;
import io.quarkus.test.security.jwt.JwtSecurity;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import jakarta.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


@QuarkusTest
@TestHTTPEndpoint(MeterResource.class)
public class MeterIntegrationTests extends TestBase {
    List<Meter> meters;
    Meter deleted_meter;

    @BeforeEach
    @Transactional
    public void setupDb() {
        meters = List.of(fakeMeter(faker, testHousehold.id), fakeMeter(faker, testHousehold.id),
                fakeMeter(faker, testHousehold.id),
                fakeMeter(faker, testHousehold.id));

        meters.forEach(meter -> meter.persist());

        deleted_meter = fakeMeter(faker, testHousehold.id);
        deleted_meter.deletedAt = LocalDateTime.now();
        deleted_meter.persist();


    }

    @AfterEach
    @Transactional
    public void cleanDb() {
        Meter.deleteAll();
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void listMetersOk() {
        given()
                .get()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", containsInAnyOrder(meters.stream().map(meter -> (int) meter.id).toArray()))
                .body("id", not(equalTo(deleted_meter.id)))
                .body("name", containsInAnyOrder(meters.stream().map(meter -> meter.name).toArray()));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void createMeterOk() {
        MeterCreateDto meterCreateDto = fakeMeterCreateDto(faker);
        int id = given().contentType(ContentType.JSON)
                .body(meterCreateDto)
                .post()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(201)
                .body("name", equalTo(meterCreateDto.name))
                .extract()
                .body()
                .path("id");

        Meter meter = Meter.findById(id);
        assertThat(meter.name, equalTo(meterCreateDto.name));
        assertThat(meter.comment, equalTo(meterCreateDto.comment));
        assertThat(meter.unit, equalTo(meterCreateDto.unit));
        assertThat(meter.type, equalTo(meterCreateDto.type));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void createMeterValid() {
        MeterCreateDto meterCreateDto = fakeMeterCreateDto(faker);
        meterCreateDto.name = "";
        given().contentType(ContentType.JSON)
                .body(meterCreateDto)
                .post()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);
    }


    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getMeterOk() {
        given().pathParam("meterId", meters.get(0).id)
                .get("/{meterId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) meters.get(0).id))
                .body("name", equalTo(meters.get(0).name));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getMeterNotFound() {
        given().pathParam("meterId", -1).get("/{meterId}").then().log().ifValidationFails().statusCode(404);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getMeterHouseholdMismatch() {
        RestAssured.given()
                .pathParam("householdId", "not-found")
                .pathParam("meterId", meters.get(0).id)
                .get("/{meterId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(404);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void updateMeterOk() {
        MeterCreateDto meterCreateDto = fakeMeterCreateDto(faker);
        given().contentType(ContentType.JSON)
                .body(meterCreateDto)
                .pathParam("meterId", meters.get(0).id)
                .put("/{meterId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) meters.get(0).id))
                .body("name", equalTo(meterCreateDto.name));

        Meter meter = Meter.findById(meters.get(0).id);
        assertThat(meter.name, equalTo(meterCreateDto.name));
        assertThat(meter.comment, equalTo(meterCreateDto.comment));
        assertThat(meter.unit, equalTo(meterCreateDto.unit));
        assertThat(meter.type, equalTo(meterCreateDto.type));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void updateMeterInvalid() {
        MeterCreateDto meterCreateDto = fakeMeterCreateDto(faker);
        meterCreateDto.name = "";
        given().contentType(ContentType.JSON)
                .body(meterCreateDto)
                .pathParam("meterId", meters.get(0).id)
                .put("/{meterId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);

        Meter meter = Meter.findById(meters.get(0).id);
        assertThat(meter.name, equalTo(meters.get(0).name));
        assertThat(meter.comment, equalTo(meters.get(0).comment));
    }


    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void deleteMeterOk() {
        given().pathParam("meterId", meters.get(0).id)
                .delete("/{meterId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) meters.get(0).id))
                .body("name", equalTo(meters.get(0).name));

        Meter meter = Meter.findById(meters.get(0).id);
        assertThat(meter.deletedAt, lessThanOrEqualTo(LocalDateTime.now()));
    }


}
