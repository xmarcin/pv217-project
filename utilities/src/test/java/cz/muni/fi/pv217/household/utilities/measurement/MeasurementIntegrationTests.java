package cz.muni.fi.pv217.household.utilities.measurement;

import static cz.muni.fi.pv217.household.utilities.measurement.MeasurementUtils.fakeMeasurement;
import static cz.muni.fi.pv217.household.utilities.measurement.MeasurementUtils.fakeMeasurementCreateDto;
import static cz.muni.fi.pv217.household.utilities.meter.MeterUtils.fakeMeter;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import cz.muni.fi.pv217.household.utilities.TestBase;
import cz.muni.fi.pv217.household.utilities.measurement.dtos.MeasurementCreateDto;
import cz.muni.fi.pv217.household.utilities.measurement.models.Measurement;
import cz.muni.fi.pv217.household.utilities.meter.models.Meter;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.ClaimType;
import io.quarkus.test.security.jwt.JwtSecurity;
import io.restassured.http.ContentType;
import jakarta.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestHTTPEndpoint(MeasurementResource.class)
public class MeasurementIntegrationTests extends TestBase {
    List<String> users = List.of("user0", "user1", "user2", "user3", "user4");

    List<Meter> meters;

    List<Measurement> measurements;

    Meter testMeter;

    private List<String> userIds(Integer... idx) {
        return Arrays.stream(idx).map(users::get).toList();
    }

    private void addMeasurement(long meterId, double value, LocalDateTime timestamp, String comment) {
        Measurement measurement = new Measurement();
        measurement.meterId = meterId;
        measurement.value = value;
        measurement.timestamp = timestamp;
        measurement.comment = comment;
        measurements.add(measurement);
    }

    @BeforeEach
    @Transactional
    public void setupDb() {
        meters = List.of(fakeMeter(faker, testHousehold.id), fakeMeter(faker, testHousehold.id),
                fakeMeter(faker, testHousehold.id),
                fakeMeter(faker, testHousehold.id));

        meters.forEach(meter -> meter.persist());
        //
        //        deleted_meter = fakeMeter(faker, testHousehold.id);
        //        deleted_meter.deletedAt = LocalDateTime.now();
        //        deleted_meter.persist();

        measurements = List.of(fakeMeasurement(faker, meters.get(1).id), fakeMeasurement(faker, meters.get(1).id),
                fakeMeasurement(faker, meters.get(1).id), fakeMeasurement(faker, meters.get(1).id),
                fakeMeasurement(faker, meters.get(1).id), fakeMeasurement(faker, meters.get(1).id));
        measurements.forEach(measurement -> measurement.persist());

    }

//    @AfterEach
//    @Transactional
//    public void cleanDb() {
//        Measurement.deleteAll();
//        Meter.deleteAll();
//    }


    //GET /{householdId}/meters/{meterId}/measurements
    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void listMeasurementsOk() {
        given().pathParam("meterId", meters.get(1).id)
                .get()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id",

                        containsInAnyOrder(measurements.stream().map(measurement -> (int) measurement.id).toArray()))
                .body("value",
                        containsInAnyOrder(
                                measurements.stream().map(measurement -> (float) measurement.value).toArray()));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void createMeasurementOk() {
        MeasurementCreateDto measurementCreateDto = fakeMeasurementCreateDto(faker);
        int id = given().contentType(ContentType.JSON).pathParam("meterId", 2)
                .body(measurementCreateDto)
                .post()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(201)
                .body("value", equalTo((float) measurementCreateDto.value))
                .extract()
                .body()
                .path("id");

        Measurement measurement = Measurement.findById(id);

        assertThat(measurement.comment, equalTo(measurementCreateDto.comment));
        assertThat(measurement.value, equalTo(measurementCreateDto.value));
        assertThat(measurement.timestamp, equalTo(measurementCreateDto.timestamp));
    }


    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void deleteMeasurementOk() {
        given().pathParam("meterId", meters.get(1).id)
                .pathParam("measurementId", measurements.get(1).id)
                .delete("/{measurementId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) measurements.get(1).id))
                .body("value", equalTo((float) measurements.get(1).value));

        Measurement measurement = Measurement.findById(measurements.get(1).id);
        assertThat(measurement.deletedAt, lessThanOrEqualTo(LocalDateTime.now()));
    }


}
