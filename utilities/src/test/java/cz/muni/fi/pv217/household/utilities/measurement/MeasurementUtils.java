package cz.muni.fi.pv217.household.utilities.measurement;


import com.github.javafaker.Faker;
import cz.muni.fi.pv217.household.utilities.measurement.dtos.MeasurementCreateDto;
import cz.muni.fi.pv217.household.utilities.measurement.models.Measurement;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;


public class MeasurementUtils {
    public static Measurement fakeMeasurement(Faker faker, long meterId) {
        Measurement measurement = new Measurement();
        measurement.value = faker.number().randomDouble(2, 1, 1000);
        measurement.timestamp = faker.date().past(30, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        measurement.meterId = meterId;
        // measurement.comment =

        return measurement;
    }

    public static MeasurementCreateDto fakeMeasurementCreateDto(Faker faker) {
        MeasurementCreateDto measurementCreateDto = new MeasurementCreateDto();
        measurementCreateDto.timestamp = faker.date().past(30, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        measurementCreateDto.value = faker.number().randomDouble(2, 1, 1000);

        return measurementCreateDto;
    }
}