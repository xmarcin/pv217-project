#!/bin/bash

set -e

PROJECTS=`find . -mindepth 1 -name 'pom.xml'`

if [ -z "$PROJECTS" ]; then
    echo "No Java projects to test"
    exit 0
fi

MODULES=`echo "$PROJECTS" | xargs dirname`

WD=${CI_PROJECT_DIR:-`git rev-parse --show-toplevel`}

for MODULE in $MODULES; do
    if ! env -C "$MODULE" mvn clean test \
            --batch-mode \
            --color always \
            --show-version; then
        FAILED=1
    fi
done

[ -z "$FAILED" ]
