#!/bin/bash

set -e

PROJECTS=`find . -mindepth 1 -name 'pom.xml'`

if [ -z "$PROJECTS" ]; then
    echo "No Java projects to check"
    exit 0
fi

MODULES=`echo "$PROJECTS" | xargs dirname`

WD=${CI_PROJECT_DIR:-`git rev-parse --show-toplevel`}

for MODULE in $MODULES; do
    cp -v $WD/scripts/java/checkstyle-pom.xml "$MODULE/checkstyle-pom.xml"
    if ! env -C "$MODULE" mvn -f "./checkstyle-pom.xml"  \
            checkstyle:check \
            --batch-mode \
            --color always \
            --show-version \
            -Dcheckstyle.violationSeverity=warning \
            -Dcheckstyle.config.location=$WD/scripts/java/checkstyle-config.xml; then
        FAILED=1
    fi
    rm -if "$MODULE/checkstyle-pom.xml"
done

[ -z "$FAILED" ]
