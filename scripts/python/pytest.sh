#!/bin/bash

set -e

PROJECTS=`find . -mindepth 1 -name 'Pipfile'`

if [ -z "$PROJECTS" ]; then
    exit 0
fi

WD=${CI_PROJECT_DIR:-`git rev-parse --show-toplevel`}

MODULES=`echo "$PROJECTS" | xargs dirname`

for MODULE in $MODULES; do
    if [ -d "$MODULE/tests" ]; then
        if ! env -C "$MODULE" pipenv run pytest; then
            FAILED=1
        fi
    fi
done

[ -z "$FAILED" ]
