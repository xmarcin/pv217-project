#!/bin/bash

set -e

PROJECTS=`find . -mindepth 1 -name 'Pipfile'`

if [ -z "$PROJECTS" ]; then
    exit 0
fi

WD=${CI_PROJECT_DIR:-`git rev-parse --show-toplevel`}

MODULES=`echo "$PROJECTS" | xargs dirname`

for MODULE in $MODULES; do
    if ! env -C "$MODULE" pipenv run pylint --rcfile $WD/scripts/python/pylint.toml .; then
        FAILED=1
    fi
done

[ -z "$FAILED" ]
