#!/bin/bash

set -e

PROJECTS=`find . -mindepth 1 -name 'Pipfile'`

if [ -z "$PROJECTS" ]; then
    exit 0
fi

MODULES=`echo "$PROJECTS" | xargs dirname`

for MODULE in $MODULES; do
    env -C "$MODULE" pipenv install -d --skip-lock
    env -C "$MODULE" pipenv install pylint==3.0.2 black==23.11.0 pytest==7.4.3 --skip-lock
done
