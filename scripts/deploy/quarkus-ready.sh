#!/bin/bash

set -e

PORT=${QUARKUS_HTTP_PORT:-8080}

TARGET=${HEALTHCHEK_TARGET:-http://localhost:$PORT}

STATUS=`curl --fail --silent "$TARGET/q/health/ready" | jq .status`

[ "$STATUS" = '"UP"' ]
