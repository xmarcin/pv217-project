#!/bin/bash

set -e

PORT=${QUARKUS_HTTP_PORT:-8080}

TARGET=${HEALTHCHEK_TARGET:-http://localhost:$PORT}

STATUS=`curl --fail --silent "$TARGET/q/health/live" | jq .status`

if [ "$1" = "kill" ]; then
    [ "$STATUS" = '"UP"' ] || kill -KILL 1
else
    [ "$STATUS" = '"UP"' ]
fi
