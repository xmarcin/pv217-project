#!/bin/bash

set -e

RSA_PRIVATE_KEY=${RSA_PRIVATE_KEY:-`mktemp ./rsaPrivateKey.XXXXXXXXXX.pem`}
PRIVATE_KEY=${PRIVATE_KEY:-privateKey.pem}
PUBLIC_KEY=${PUBLIC_KEY:-publicKey.pem}

function clean_up {
    rm -f ${RSA_PRIVATE_KEY}
}
trap clean_up EXIT

set -x

openssl genrsa -out ${RSA_PRIVATE_KEY} 2048
openssl rsa -pubout -in ${RSA_PRIVATE_KEY} -out ${PUBLIC_KEY}
openssl pkcs8 -topk8 -nocrypt -inform pem -in ${RSA_PRIVATE_KEY} -outform pem -out ${PRIVATE_KEY}
