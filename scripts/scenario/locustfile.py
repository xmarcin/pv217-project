"""Locust file script"""
from os import environ
from secrets import token_hex

from locust import HttpUser, task
from requests.auth import AuthBase
from requests.sessions import Request


USERS_URL = environ.get("LOCUST_USERS_URL", "http://localhost:8080")
HOUSEHOLDS_URL = environ.get("LOCUST_HOUSEHOLDS_URL", "http://localhost:8081")
UTILITIES_URL = environ.get("LOCUST_UTILITIES_URL", "http://localhost:8082")
CHORES_URL = environ.get("LOCUST_CHORES_URL", "http://localhost:8083")
EXPENSES_URL = environ.get("LOCUST_EXPENSES_URL", "http://localhost:8084")


class HTTPBearerAuth(AuthBase):
    """Bearer token requests authenticator"""

    def __init__(self, jwt: str) -> None:
        self.jwt = jwt

    def __call__(self, request: Request) -> Request:
        request.headers["Authorization"] = f"Bearer {self.jwt}"
        return request


class HouseholdUser(HttpUser):
    """Test User of the households system"""

    def __init__(self, *args, **kwargs) -> None:
        self.email: str = ""
        self.password: str = ""
        self.jwt: str = ""
        self.refresh_token: str = ""
        self.auth: AuthBase | None = None
        self.user_id: str = ""
        self.household_id: str = ""
        self.meter_id: str = ""
        self.chore_id: str = ""
        self.host = "http://localhost:8080"
        super().__init__(*args, **kwargs)

    def user_create(self) -> None:
        """Creates a new user"""
        self.email = f"{token_hex(8)}@example.com"
        self.password = "password"
        resp = self.client.post(
            f"{USERS_URL}/users",
            json={
                "bankaccount": 123456789,
                "birthday": "2023-12-09T16:06:28.578Z",
                "email": self.email,
                "firstname": token_hex(8),
                "lastname": token_hex(8),
                "password": self.password,
            },
        )
        self.user_id = resp.json()["id"]

    def user_login(self) -> None:
        """Logins the user"""
        resp = self.client.post(
            f"{USERS_URL}/auth/login", auth=(self.email, self.password)
        )
        resp_json = resp.json()
        self.jwt = resp_json["jwt"]
        self.refresh_token = resp_json["refresh"]
        self.auth = HTTPBearerAuth(self.jwt)

    def household_create(self) -> None:
        """Creates a new household for the user"""
        resp = self.client.post(
            f"{HOUSEHOLDS_URL}/households",
            json={
                "name": token_hex(8),
                "accountNumber": token_hex(8),
                "address": token_hex(8),
            },
            auth=self.auth,
        )
        self.household_id = resp.json()["id"]

    def utility_meter_create(self) -> None:
        """Creates a new utility meter in the household"""
        resp = self.client.post(
            f"{UTILITIES_URL}/{self.household_id}/meters",
            json={
                "name": token_hex(8),
                "comment": token_hex(16),
                "type": "Water",
                "unit": "m^3",
            },
            auth=self.auth,
        )
        self.meter_id = resp.json()["id"]

    def chore_create(self) -> None:
        """Creates a new chore in the household"""
        resp = self.client.post(
            f"{CHORES_URL}/chores",
            json={
                "householdId": self.household_id,
                "name": token_hex(8),
                "description": token_hex(16),
                "frequency": "DAILY",
                "recurrence": 1,
                "durationMinutes": 60,
            },
            auth=self.auth,
        )
        self.chore_id = resp.json()["id"]

    def on_start(self) -> None:
        """Start handler"""
        self.user_create()
        self.user_login()
        self.household_create()
        self.utility_meter_create()
        self.chore_create()
        return super().on_start()

    # Users service tasks

    @task(100)
    def user_get(self) -> None:
        """Tests user listing"""
        self.client.get(f"{USERS_URL}/users", auth=self.auth)

    @task(20)
    def user_update(self) -> None:
        """Updates the user"""
        self.client.patch(
            f"{USERS_URL}/users/{self.user_id}",
            json={"firstname": token_hex(8), "lastname": token_hex(8)},
            auth=self.auth,
        )

    @task(1)
    def user_login_refresh(self) -> None:
        """Refershes the login token"""
        resp = self.client.post(
            f"{USERS_URL}/auth/refresh", json={"refresh": self.refresh_token}
        )
        resp_json = resp.json()
        self.jwt = resp_json["jwt"]
        self.refresh_token = resp_json["refresh"]
        self.auth = HTTPBearerAuth(self.jwt)

    # Households service tasks

    @task(100)
    def households_list(self) -> None:
        """Lists user's households"""
        self.client.get(f"{HOUSEHOLDS_URL}/households", auth=self.auth)

    @task(20)
    def household_update(self) -> None:
        """Updates the household"""
        self.client.put(
            f"{HOUSEHOLDS_URL}/households/{self.household_id}",
            json={
                "name": token_hex(8),
                "accountNumber": token_hex(8),
                "address": token_hex(8),
            },
            auth=self.auth,
        )

    # Chores service tasks

    @task(100)
    def chores_list(self) -> None:
        """Lists chores in the household"""
        self.client.get(
            f"{CHORES_URL}/chores/household/{self.household_id}", auth=self.auth
        )

    @task(100)
    def chore_assignments_list(self) -> None:
        """Lists chores assignments in household"""
        with self.client.get(
            f"{CHORES_URL}/chores-assignment/household/{self.household_id}/completed",
            auth=self.auth,
            catch_response=True,
        ) as resp:
            if resp.status_code == 404:
                resp.success()

    @task(70)
    def chore_assignment_create(self) -> None:
        """Creates a new chore assignment in the household"""
        resp = self.client.post(
            f"{CHORES_URL}/chores-assignment/assign",
            json={
                "choreId": self.chore_id,
                "memberId": self.user_id,
                "startDate": "2023-12-09T16:06:28",
            },
            auth=self.auth,
        )
        self.client.put(
            f"{CHORES_URL}/chores-assignment/{resp.json()['id']}/completed",
            auth=self.auth,
        )

    # Utilities service tasks

    @task(100)
    def utilities_meters_list(self) -> None:
        """Lists utility meters in the household"""
        self.client.get(f"{UTILITIES_URL}/{self.household_id}/meters", auth=self.auth)

    @task(100)
    def utilities_measurements_list(self) -> None:
        """Lists measurements with the meter"""
        self.client.get(
            f"{UTILITIES_URL}/{self.household_id}/meters/{self.meter_id}/measurements",
            auth=self.auth,
        )

    @task(70)
    def utilities_measurement_add(self) -> None:
        """Add a new measurement to the meter"""
        self.client.post(
            f"{UTILITIES_URL}/{self.household_id}/meters/{self.meter_id}/measurements",
            json={
                "value": 100,
                "comment": token_hex(8),
                "timestamp": "2022-03-10T12:15:50",
            },
            auth=self.auth,
        )

    # Expenses service tasks

    @task(100)
    def expenses_list(self) -> None:
        """Lists transactions in the household"""
        self.client.get(
            f"{EXPENSES_URL}/{self.household_id}/transactions", auth=self.auth
        )

    @task(100)
    def expenses_balance(self) -> None:
        """Calculates balances in the household"""
        self.client.get(f"{EXPENSES_URL}/{self.household_id}/balance", auth=self.auth)

    @task(70)
    def expenses_transaction_create(self) -> None:
        """Creates a new transaction in the household"""
        self.client.post(
            f"{EXPENSES_URL}/{self.household_id}/transactions",
            json={
                "name": token_hex(8),
                "comment": token_hex(16),
                "amount": 100,
                "payers": [{"userId": self.user_id, "proportion": 1}],
                "beneficiaries": [{"userId": self.user_id, "proportion": 1}],
            },
            auth=self.auth,
        )
