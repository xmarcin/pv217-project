"""Test configuration module"""
import pytest
from households_users import create_app
import households_users.database.utils


@pytest.fixture()
def app():
    """Testing Flask application factory"""
    test_app = create_app()
    test_app.config.update(
        {
            "TESTING": True,
        }
    )

    yield test_app
    households_users.database.utils.drop_tables()
    households_users.database.utils.database_close()
    # with app.app_context():
    #     households_users.database.utils._database_close()
