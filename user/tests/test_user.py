"""Tests for the User Resource"""
import datetime

from jwcrypto.jwt import uuid
from flask import Flask

from households_users.database.utils import User, RefreshToken


def test_user_create_ok(app: Flask) -> None:
    """Tests user creation"""
    response = app.test_client().post(
        "/users",
        json={
            "birthday": "2023-12-04T23:13:08.092Z",
            "email": "example@example.net",
            "firstname": "Alice",
            "lastname": "Smith",
            "password": "UltraSecretPassword123",
        },
    )
    assert response.status_code == 200
    assert response.json is not None
    assert response.json["email"] == "example@example.net"
    assert response.json["firstname"] == "Alice"
    assert response.json["bankaccount"] is None
    db_user = User.get_by_id(response.json["id"])
    assert db_user is not None
    assert db_user.email == "example@example.net"


def test_user_create_ok_bankaccount(app: Flask) -> None:
    """Tests user creation"""
    response = app.test_client().post(
        "/users",
        json={
            "birthday": "2023-12-04T23:13:08.092Z",
            "email": "example2@example.net",
            "firstname": "Alice",
            "lastname": "Smith",
            "password": "UltraSecretPassword123",
            "bankaccount": "123456789",
        },
    )
    assert response.status_code == 200
    assert response.json is not None
    assert response.json["email"] == "example2@example.net"
    assert response.json["firstname"] == "Alice"
    assert response.json["bankaccount"] == "123456789"
    db_user = User.get_by_id(response.json["id"])
    assert db_user is not None
    assert db_user.email == "example2@example.net"


def test_user_create_email(app: Flask) -> None:
    """Tests user creation"""
    response = app.test_client().post(
        "/users",
        json={
            "birthday": "2023-12-04T23:13:08.092Z",
            "email": "not.valid",
            "firstname": "Alice",
            "lastname": "Smith",
            "password": "UltraSecretPassword123",
        },
    )

    assert response.status_code == 400
    assert response.json is not None
    assert response.json["error"] == "Not a valid email address."


def test_user_get_ok(app: Flask) -> None:
    """Test get user info."""
    my_uuid = uuid.uuid4()
    User.create(
        id=my_uuid,
        firstname="Test",
        lastname="Surname",
        bankaccount="12345678",
        password="$argon2id$v=19$m=65536,t=3,p=4$AQtTk9NJMKJzNU\
        HiM7t5zA$jUmVKruXeXdFgWJqGa4XJcKHELjFiwGe5EFeQQfhyMc",
        email="example1@example.net",
        registeredat=datetime.datetime.now(),
        birthday="2022-12-20",
    )
    RefreshToken.create(
        secret_key=b"AAAAAAAABBBBBBBB",
        user=my_uuid,
        issued=datetime.datetime.now(),
        expire=datetime.datetime.now() + datetime.timedelta(days=1),
    )

    response = app.test_client().post(
        "/auth/refresh",
        json={
            "refresh": b"AAAAAAAABBBBBBBB".hex(),
        },
    )

    assert response is not None
    assert response.status_code == 200
    assert response.json is not None

    response2 = app.test_client().get(
        "/users",
        headers={"Authorization": f"Bearer {response.json['jwt']}"},
    )
    assert response2 is not None
    assert response2.status_code == 200
    assert response2.json is not None
    assert response2.json["bankaccount"] == "12345678"
    assert response2.json["birthday"] == "Tue, 20 Dec 2022 00:00:00 GMT"
    assert response2.json["email"] == "example1@example.net"
    assert response2.json["firstname"] == "Test"
    assert response2.json["lastname"] == "Surname"


def test_user_delete_ok(app: Flask) -> None:
    """Test deletion of user."""
    my_uuid = uuid.uuid4()
    User.create(
        id=my_uuid,
        firstname="Test",
        lastname="Surname",
        bankaccount="12345678",
        password="$argon2id$v=19$m=65536,t=3,p=4$AQtTk9NJMKJzNUHiM7t5zA$jUmVKru\
        XeXdFgWJqGa4XJcKHELjFiwGe5EFeQQfhyMc",
        email="example1@example.net",
        registeredat=datetime.datetime.now(),
        birthday="2022-12-20",
    )
    RefreshToken.create(
        secret_key=b"AAAAAAAABBBBBBBB",
        user=my_uuid,
        issued=datetime.datetime.now(),
        expire=datetime.datetime.now() + datetime.timedelta(days=1),
    )

    response = app.test_client().post(
        "/auth/refresh",
        json={
            "refresh": b"AAAAAAAABBBBBBBB".hex(),
        },
    )

    assert response is not None
    assert response.status_code == 200
    assert response.json is not None

    response2 = app.test_client().delete(
        f"/users/{my_uuid}",
        headers={"Authorization": f"Bearer {response.json['jwt']}"},
    )
    assert response2 is not None
    assert response2.status_code == 200
    assert response2.json is not None

    db_user = User.get_by_id(my_uuid)
    assert db_user is not None
    assert db_user.email != "example2@example.net"
    assert db_user.password == "[DELETED]"
    assert db_user.firstname == "[DELETED]"
    assert db_user.lastname == "[DELETED]"
