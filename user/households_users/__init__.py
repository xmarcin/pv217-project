"""Main entry point of the application."""
# https://peewee.readthedocs.io/en/latest/peewee/quickstart.html
# https://dev.to/swarnimwalavalkar/build-and-deploy-a-rest-api-microservice-with-python-flask-and-docker-5c2d

from flask import Flask
from flask_restful import Api
from flasgger import Swagger
from prometheus_flask_exporter import PrometheusMetrics

# from flask_restful_swagger_2 import Api as SwaggerApi

import households_users.database.utils
import households_users.resource.users as RU
import households_users.resource.auth as RA

# from flask import HTTPBasicAuth


def create_app():
    """Creates a new flask app."""
    print("db connect...")
    households_users.database.utils.database_connect()
    print("db setup...")
    households_users.database.utils.create_tables()
    print("db done!")

    # Initialize Flask
    app = Flask(__name__)
    api = Api(app)
    Swagger(
        app,
        template={
            "securityDefinitions": {
                "basicAuth": {"type": "basic"},
                "bearerToken": {
                    "type": "apiKey",
                    "name": "Authorization",
                    "in": "header",
                    "description": "Use 'Bearer ' prefix, e.g. 'Bearer abcde12345'",
                },
            },
        },
    )
    PrometheusMetrics(app)

    api.add_resource(RU.Users, "/users")
    api.add_resource(RU.UserRoute, "/users/<string:user_id>")
    api.add_resource(RA.AuthLogin, "/auth/login")
    api.add_resource(RA.AuthRefresh, "/auth/refresh")

    return app
