"""Utilities for database operation. Includes Models."""
import os
from datetime import datetime, timedelta
from argon2 import PasswordHasher
from peewee import Model, PostgresqlDatabase, CharField, DateField, BlobField, UUIDField

REFRESH_LENGTH = 16

db_name = os.environ.get("DB_NAME", "my_app")
db_user = os.environ.get("DB_USER", "postgres")
db_password = os.environ.get("DB_PASSWORD", "password")
db_host = os.environ.get("DB_HOST", "127.0.0.1")
db_port = int(os.environ.get("DB_PORT", "5432"))

db = PostgresqlDatabase(
    db_name, user=db_user, password=db_password, host=db_host, port=db_port
)


def database_connect():
    """Connect to database."""
    return db.connect()


def database_close():
    """Disconnect the database."""
    return db.close()


def create_tables():
    """Create database table scheme."""
    db.create_tables([User, RefreshToken])


def drop_tables():
    """Drops database table scheme."""
    db.drop_tables([User, RefreshToken])


class User(Model):
    """Peewee model for storing User."""

    id = UUIDField(primary_key=True)
    # id = IntegerField(primary_key=True)
    firstname = CharField()
    lastname = CharField()
    bankaccount = CharField(null=True)
    birthday = DateField()
    password = CharField()
    email = CharField(unique=True)
    registeredat = DateField()
    deletedAt = DateField(null=True)

    class Meta:
        """Peewee Model for refresh token."""

        database = db


def _validate_user(email: str, password: str):
    try:
        selected_user = User.get((User.email == email) & (User.deletedAt.is_null()))
    except Exception as e:
        print(f"Validation failed: {e}")
        return False

    try:
        PasswordHasher().verify(selected_user.password, password)
        return selected_user.id
    except Exception as e:
        print(f"DEBUG: Failed passwordhasher {e}")
        return False


def _user_soft_delete(selected_user):
    # try:
    #     selected_user = User.get(User.id == UUID)
    # except Exception as e:
    #     print(f"Validation failed: {e}")
    #     return False

    selected_user.deletedAt = datetime.now()
    selected_user.email = f"[DELETED]{selected_user.id}"
    selected_user.password = "[DELETED]"
    selected_user.bankaccount = "[DELETED]"
    selected_user.firstname = "[DELETED]"
    selected_user.lastname = "[DELETED]"
    selected_user.save()


class RefreshToken(Model):
    """Peewee Model for refresh token."""

    # id = AutoField(primary_key=True)
    secret_key = BlobField(unique=True, primary_key=True)
    user = UUIDField()
    issued = DateField()
    expire = DateField()

    class Meta:
        """Use default db."""

        database = db


def _generate_refresh(uuid: str, expiry: timedelta):
    secret = os.urandom(REFRESH_LENGTH)
    # print(f"created secret key {secret}")
    now = datetime.now()
    exp = now + expiry

    try:
        query = RefreshToken.delete().where(RefreshToken.user == uuid)
        deleted_count = query.execute()
        print(f"DEBUG: deleted {deleted_count} for {uuid}")
    except Exception as e:
        print(f"DEBUG: Unexpected error {e}!")

    try:
        RefreshToken.create(secret_key=secret, issued=now, expire=exp, user=uuid)
    except BaseException:
        return None
    return {"refresh": secret, "exp": exp}


def _refresh_delete(uuid):
    try:
        query = RefreshToken.delete().where(RefreshToken.user == uuid)
        deleted_count = query.execute()
        print(f"DEBUG: deleted {deleted_count} for {uuid}")
    except Exception as e:
        print(f"DEBUG: Unexpected error {e}!")
        return False
    return True


def _refresh_is_valid(secret_key):
    try:
        selected_user = RefreshToken.get(
            (RefreshToken.secret_key == secret_key)
            & (RefreshToken.expire > datetime.now())
        )
    except Exception as e:
        print(f"DEBUG: refresh_is_valid: {e}")
        return False
    return selected_user.user
