"""Module for auth endpoint."""
import sys
from datetime import timedelta
from flask import jsonify, request, Response
from flask_restful import Resource, reqparse

from households_users.common.kjwt import generate_jwt

from ..database.utils import (
    _generate_refresh,
    _refresh_is_valid,
    _validate_user,
    _refresh_delete,
)


REFRESH_TIMEOUT = 24  # in hours


def _process_refresh(user):
    return _generate_refresh(user, timedelta(hours=REFRESH_TIMEOUT))


class AuthLogin(Resource):
    """Flask class for managing auth/login route."""

    def __init__(self):
        """Initialize."""
        self.reqparse = reqparse.RequestParser()

    def post(self):
        """
        Post Auth Login request.
        ---
        tags:
          - auth
        security:
          - basicAuth: []
        responses:
          200:
            description: OK.
            schema:
              properties:
                jwt:
                  type: string
                  description: JWT.
                  example: "eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MDE2OTAyOTUsImlhdC
                    I6MTcwMTY4OTY5NSwic3ViIjoiMmYyNzM1OGItOTM4Yy00MDYzLWIyNjEtZ
                    TQ2Y2ZhYTRmNzFlIn0.EC1uIiC7hdlZ_S2PrHJpmBazbEAwqyoIATZMp-ef
                    hZ8"
                refresh:
                  type: string
                  description: Refresh token for /auth/refresh endpoint.
                    Has greater validity than JWT. Hex encoded
                  example: "97d712fe2690ffd051eca6a6146b5b55"
                refresh_exp:
                  type: string
                  format: date
                  description: Expire of the refresh token
                  example: "Tue, 05 Dec 2023 12:34:55 GMT"
          401:
            description: Authentication information is missing or invalid
            headers:
              WWW_Authenticate:
                schema:
                  type: string
        """

        if (
            not request.authorization
            or not request.authorization.username
            or not request.authorization.password
        ):
            return Response(
                "Authentication required.",
                401,
                {"WWW-Authenticate": "Basic realm='Login Required'"},
            )

        username = request.authorization.username
        password = request.authorization.password
        print(f"Username: {username}, password: {password}")
        user_id = _validate_user(username, password)
        if not user_id:
            return {"error": "Authentication failed, invalid credentials"}, 401

        refresh_token = _process_refresh(user=user_id)
        if not refresh_token:
            print("Could not generate SECRET KEY for jwt", file=sys.stderr)
            return {"error": "Internal server error"}, 500

        e = generate_jwt(user_id)
        return jsonify(
            {
                "jwt": e,
                "refresh": refresh_token["refresh"].hex(),
                "refresh_exp": refresh_token["exp"],
            }
        )


class AuthRefresh(Resource):
    """Flask class for managing auth/refresh route."""

    def __init__(self):
        """Initialize."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("refresh", type=str, location="json")

    def post(self):
        """
        Post call.

        Needs refresh token and returns refreshed jwt+refresh token.
        ---
        tags:
          - auth
        parameters:
          - in: body
            name: body
            description: JSON parameters.
            schema:
              properties:
                refresh:
                  description: The provided refresh token when logging in.
                  type: string
                  example: "97d712fe2690ffd051eca6a6146b5b55"
                  required: true
        responses:
          200:
            description: OK.
            schema:
              properties:
                jwt:
                  type: string
                  description: JWT.
                  example: "eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MDE2OTAyOTUsImlhdC
                    I6MTcwMTY4OTY5NSwic3ViIjoiMmYyNzM1OGItOTM4Yy00MDYzLWIyNjEtZ
                    TQ2Y2ZhYTRmNzFlIn0.EC1uIiC7hdlZ_S2PrHJpmBazbEAwqyoIATZMp-ef
                    hZ8"
                refresh:
                  type: string
                  description: Refresh token for /auth/refresh endpoint.
                    Has greater validity than JWT. Hex encoded
                  example: "97d712fe2690ffd051eca6a6146b5b55"
                refresh_exp:
                  type: string
                  format: date
                  description: Expire of the refresh token
                  example: "Tue, 05 Dec 2023 12:34:55 GMT"
          404:
            description: No valid token

        """
        args = self.reqparse.parse_args()
        print(f"DEBUG: AUTHORIZATION: {request.authorization}")
        refresh = args.get("refresh")
        if not refresh:
            return {"error": "No valid token"}, 404

        user = _refresh_is_valid(bytes.fromhex(refresh))
        if not user:
            return {"error": "No valid token"}, 404
        e = generate_jwt(user)

        refresh_token = _process_refresh(user=user)
        if not refresh_token:
            print("Could not generate SECRET KEY for jwt", file=sys.stderr)
            return {"error": "Internal server error"}, 500

        return jsonify(
            {
                "jwt": e,
                "refresh": refresh_token["refresh"].hex(),
                "refresh_exp": refresh_token["exp"],
            }
        )

    def delete(self):
        """
        Delete the refresh token.

        ---
        tags:
          - auth
        parameters:
          - in: body
            name: body
            description: JSON parameters.
            schema:
              properties:
                refresh:
                  description: The provided refresh token when logging in.
                  type: string
                  example: "97d712fe2690ffd051eca6a6146b5b55"
                  required: true
        responses:
          200:
            description: OK. Deleted token.
          404:
            description: No valid token given.
        """
        args = self.reqparse.parse_args()
        print(f"DEBUG: AUTHORIZATION: {request.authorization}")
        refresh = args.get("refresh")
        if not refresh:
            return {"error": "No valid token"}, 404

        user = _refresh_is_valid(bytes.fromhex(refresh))
        if not user:
            return {"error": "No valid token"}, 404

        if not _refresh_delete(user):
            return {"error": "Internal server error."}, 500

        return {"msg": "Token deleted"}
