"""Module for managing users end-point."""
import datetime
from argon2 import PasswordHasher
from flask import jsonify, request
from flask_restful import Resource, reqparse, inputs
from jwcrypto.jwt import uuid
import peewee

from playhouse.shortcuts import model_to_dict


from ..database.utils import User, _user_soft_delete
from ..common.kjwt import validate_jwt
from ..common.validate import is_email_valid


class UserRoute(Resource):
    """Flask class for managing user/:id route."""

    def __init__(self):
        """Initliaze class with json parser."""
        # Initialize The Flsak Request Parser and add arguments as in an
        # expected request
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("firstname", type=str, location="json")
        self.reqparse.add_argument("lastname", type=str, location="json")
        self.reqparse.add_argument("bankaccount", type=str, location="json")
        self.reqparse.add_argument("email", type=str, location="json")
        self.reqparse.add_argument("password", type=str, location="json")
        self.reqparse.add_argument(
            "birthday", type=inputs.datetime_from_iso8601, location="json"
        )

    def patch(self, user_id):
        """
        Patch for updating user information.

        ---
        tags:
          - user
        security:
          - bearerToken: []
        parameters:
          - in: path
            name: user_id
            description: UUID of the user
            type: string
            required: true
          - in: body
            name: body
            description: JSON parameters.
            schema:
              properties:
                firstname:
                  type: string
                  description: First name.
                  example: Alice
                lastname:
                  type: string
                  description: Last name.
                  example: Smith
                email:
                  type: string
                  description: Email.
                  example: example@example.net
                password:
                  type: string
                  description: Password.
                  example: UltraSecretPassword123
                birthday:
                  type: string
                  format: date-time
                  description: Date of birth.
                bankaccount:
                  type: string
                  description: Bank account number.
                  example: 123456789
        responses:
          200:
            description: OK.
          400:
            description: Not a valid argument.
        """
        print(user_id)
        args = self.reqparse.parse_args()

        try:
            user = User.get(User.id == user_id)
        except BaseException:
            return {"error": "User not found"}, 404

        auth_header = request.headers.get("Authorization")

        print(f"DEBUG: AUTHORIZATION: {type(request.authorization)}")
        valid = validate_jwt(auth_header)
        if not valid:
            return {"error": "Not valid authentization"}, 403
        if valid == -1:
            return {"error": "Expired Authentization"}, 403

        print(valid)
        if valid["sub"] != user_id:
            return {"error", "Invalid user combination"}, 400

        try:
            User.get(User.id == valid["sub"])
        except BaseException:
            return {"error": "Internal server error or something"}, 500

        if args["email"] is not None and not is_email_valid(args["email"]):
            return {"error": "Not a valid email address."}, 400
        # Update user attributes based on provided arguments
        updated_fields = []
        for key, value in args.items():
            if value is not None:
                print(f"updating {key}:{value}")
                if key == "password":
                    value = PasswordHasher().hash(value)
                setattr(user, key, value)
                updated_fields.append({key: value})

        print(user.save())

        return jsonify(model_to_dict(user))

    def delete(self, user_id):
        """
        Soft delete the user.

        Requires authentication. Data is redacted with placeholder string.

        ---
        tags:
          - user
        security:
          - bearerToken: []
        parameters:
          - in: path
            name: user_id
            description: UUID of the user
            type: string
            required: true
        responses:
          200:
            description: OK.
          400:
            description: Not a valid argument.

        """
        print(user_id)
        # args = self.reqparse.parse_args() #

        try:
            user = User.get(User.id == user_id)
        except BaseException:
            return {"error": "User not found"}, 404

        auth_header = request.headers.get("Authorization")

        print(f"DEBUG: AUTHORIZATION: {type(request.authorization)}")
        valid = validate_jwt(auth_header)
        if not valid:
            return {"error": "Not valid authentization"}, 403
        if valid == -1:
            return {"error": "Expired Authentization"}, 403

        print(valid)
        if valid["sub"] != user_id:
            return {"error", "Invalid user combination"}, 400

        try:
            User.get(User.id == valid["sub"])
        except BaseException:
            return {"error": "Internal server error or something"}, 500

        _user_soft_delete(user)
        return {"info": "ok"}


class Users(Resource):
    """Flask class for managing /users route."""

    def __init__(self):
        """Initliaze class with json parser."""
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("firstname", type=str, location="json")
        self.reqparse.add_argument("lastname", type=str, location="json")
        self.reqparse.add_argument("bankaccount", type=str, location="json")
        self.reqparse.add_argument("email", type=str, location="json")
        self.reqparse.add_argument("password", type=str, location="json")
        self.reqparse.add_argument(
            "birthday", type=inputs.datetime_from_iso8601, location="json"
        )

        super().__init__()

    def get(self):
        """
        Get call.

        Needs basic Authentication and returns information about the user.
        ---
        tags:
          - user
        security:
          - bearerToken: []
        responses:
          200:
            description: OK.
            schema:
              properties:
                firstname:
                  type: string
                  description: First name.
                  example: Alice
                lastname:
                  type: string
                  description: Last name.
                  example: Smith
                email:
                  type: string
                  description: Email.
                  example: example@example.net
                password:
                  type: string
                  description: Password.
                  example: UltraSecretPassword123
                birthday:
                  type: string
                  format: date-time
                  description: Date of birth.
                bankaccount:
                  type: string
                  description: Bank account number.
                  example: 123456789
                  nullable: true
                registeredat:
                  type: string
                  format: date-time
                  description: Date of birth.
                  example: "Mon, 04 Dec 2023 12:34:32 GMT"
                deletedat:
                  type: string
                  format: date-time
                  example: null
                  nullable: true
          400:
            description: Not a valid argument.
        """
        auth_header = request.headers.get("Authorization")

        print(f"DEBUG: AUTHORIZATION: {type(request.authorization)}")
        valid = validate_jwt(auth_header)
        if not valid:
            return {"error": "Not valid authentization"}, 403
        if valid == -1:
            return {"error": "Expired Authentization"}, 403

        print(valid)

        try:
            users = User.get(User.id == valid["sub"])
        except BaseException:
            return {"error": "Internal server error or something"}, 500

        return jsonify(model_to_dict(users))

    def post(self):
        """
        Post call. Creates new account.

        ---
        tags:
          - user
        parameters:
          - in: body
            name: body
            description: JSON parameters.
            schema:
              properties:
                firstname:
                  type: string
                  description: First name.
                  example: Alice
                  required: true
                lastname:
                  type: string
                  description: Last name.
                  example: Smith
                  required: true
                email:
                  type: string
                  description: Email.
                  example: example@example.net
                  required: true
                password:
                  type: string
                  description: Password.
                  example: UltraSecretPassword123
                  required: true
                birthday:
                  type: string
                  format: date-time
                  description: Date of birth.
                  required: true
                bankaccount:
                  type: string
                  description: Bank account number.
                  example: 123456789
        responses:
          200:
            description: OK.
          400:
            description: Not a valid argument.

        """
        args = self.reqparse.parse_args()
        if not is_email_valid(args["email"]):
            return {"error": "Not a valid email address."}, 400

        passhash = PasswordHasher().hash(args["password"])
        try:
            new_user = User.create(
                id=uuid.uuid4(),
                firstname=args["firstname"],
                lastname=args["lastname"],
                bankaccount=args.get("bankaccount", None),
                password=passhash,
                email=args["email"],
                registeredat=datetime.datetime.now(),
                birthday=args["birthday"],
            )
            return jsonify(model_to_dict(new_user))
        except peewee.IntegrityError:
            return {"error": "E-mail is already used"}, 400
