#!/usr/bin/env python3
"""Common functions for validation of data."""

import re


def is_email_valid(email):
    """
    Check if the provided string is a valid email address.

    Parameters:
    - email (str): The string to be checked for email validity.

    Returns:
    - bool: True if the email address is valid, False otherwise.

    Example:
    >>> is_valid_email("example@email.com")
    True

    >>> is_valid_email("invalid_email")
    False
    """
    # Regular expression for a basic email validation
    regex = re.compile(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$")

    # Use the match method to check if the provided email matches the pattern
    match = regex.match(email)

    # If there is a match, the email is valid; otherwise, it's not
    return bool(match)
