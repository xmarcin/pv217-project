"""Common functions for JWT."""
import os
import json
from datetime import datetime, timedelta
from jwcrypto import jwk, jwt


def _load_jwk():
    kty = os.environ.get("SECRET_TYPE", "oct")
    exp_delta = int(os.environ.get("SECRET_EXPIRE_MINUTES", 10))
    if kty == "oct":
        key = jwk.JWK(
            kty="oct",
            k=os.environ.get(
                "SECRET_KEY", "ebBM8gr_jv4rfeqGpWdN--ADNXR1QOix9cvQEYaiHnU"
            ),
        )
        return key, key, "HS256", exp_delta
    if kty == "RSA":
        with open(os.environ["SECRET_PRIVATE_FILE"], "rb") as file:
            private = jwk.JWK.from_pem(file.read())
            print(private)
        with open(os.environ["SECRET_PUBLIC_FILE"], "rb") as file:
            public = jwk.JWK.from_pem(file.read())
        return private, public, "RS256", exp_delta
    raise ValueError("Unknown JWT key type")


SECRET_SIGN, SECRET_VERIFY, SECRET_ALG, SECRET_EXPIRE_MINUTES = _load_jwk()


def validate_jwt(bearer):
    """Validate JWT from bearer HTTP header."""
    print(f"AAAAAAAAAAAAAAAAA {bearer}")
    if bearer is None:
        print("DEBUG: No bearer")
        return None
    if not bearer:
        return None
    if not bearer.lower().startswith("bearer "):
        return None

    token = bearer.split(" ")[1]

    print(bearer)

    try:
        sign_token = jwt.JWT(key=SECRET_VERIFY, jwt=token, expected_type="JWS")
    except jwt.JWTExpired as exp:
        print(f"DEBUG: expired jwt {exp}")
        return -1
    except Exception as e:
        print(f"DEBUG: exception jwt {e}")
        return None

    if not sign_token.claims:
        print("NOT HERE")
        return None
    loaded = json.loads(sign_token.claims)
    return loaded


def generate_jwt(uuid):
    """Generate JWT for user uuid."""
    iat = datetime.now()
    exp = iat + timedelta(minutes=SECRET_EXPIRE_MINUTES)

    token = jwt.JWT(
        header={"alg": SECRET_ALG},
        claims={
            "sub": f"{uuid}",
            "iat": int(iat.timestamp()),
            "exp": int(exp.timestamp()),
        },
    )

    token.make_signed_token(SECRET_SIGN)

    e = token.serialize()
    return e
