package cz.muni.fi.pv217.household.expense.transaction;

import com.github.javafaker.Faker;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionCreateDto;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionSideCreateDto;
import cz.muni.fi.pv217.household.expense.transaction.models.Transaction;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSide;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSideType;
import java.util.List;

public class TransactionUtils {

    public static Transaction fakeTransaction(Faker faker, String householdId) {
        Transaction transaction = new Transaction();
        transaction.name = faker.commerce().productName();
        transaction.comment = faker.commerce().productName();
        transaction.amount = faker.number().randomDouble(2, 1, 1000);
        transaction.householdId = householdId != null ? householdId : faker.internet().uuid();
        transaction.authorId = faker.internet().uuid();
        transaction.sides = List.of(fakeTransactionSide(faker, TransactionSideType.PAYER),
                fakeTransactionSide(faker, TransactionSideType.BENEFICIARY));
        return transaction;
    }

    public static TransactionSide fakeTransactionSide(Faker faker, TransactionSideType type) {
        TransactionSide transactionSide = new TransactionSide();
        transactionSide.userId = faker.internet().uuid();
        transactionSide.type = type;
        transactionSide.proportion = 1.0;
        return transactionSide;
    }

    public static TransactionCreateDto fakeTransactionCreateDto(Faker faker) {
        TransactionCreateDto transactionCreateDto = new TransactionCreateDto();
        transactionCreateDto.name = faker.commerce().productName();
        transactionCreateDto.comment = faker.commerce().productName();
        transactionCreateDto.amount = faker.number().randomDouble(2, 1, 1000);
        transactionCreateDto.payers = List.of(fakeTransactionSideCreateDto(faker));
        transactionCreateDto.beneficiaries = List.of(fakeTransactionSideCreateDto(faker));
        return transactionCreateDto;
    }

    public static TransactionSideCreateDto fakeTransactionSideCreateDto(Faker faker) {
        TransactionSideCreateDto transactionSideCreateDto = new TransactionSideCreateDto();
        transactionSideCreateDto.userId = "user" + faker.number().numberBetween(0, 4);
        transactionSideCreateDto.proportion = 1.0;
        return transactionSideCreateDto;
    }
}
