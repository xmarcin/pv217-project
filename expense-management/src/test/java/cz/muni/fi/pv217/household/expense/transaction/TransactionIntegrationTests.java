package cz.muni.fi.pv217.household.expense.transaction;

import static cz.muni.fi.pv217.household.expense.transaction.TransactionUtils.fakeTransaction;
import static cz.muni.fi.pv217.household.expense.transaction.TransactionUtils.fakeTransactionCreateDto;
import static cz.muni.fi.pv217.household.expense.transaction.TransactionUtils.fakeTransactionSideCreateDto;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;

import cz.muni.fi.pv217.household.expense.TestsBase;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionCreateDto;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionUpdateDto;
import cz.muni.fi.pv217.household.expense.transaction.models.Transaction;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSide;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSideType;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.ClaimType;
import io.quarkus.test.security.jwt.JwtSecurity;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestHTTPEndpoint(TransactionResource.class)
public class TransactionIntegrationTests extends TestsBase {

    List<Transaction> transactions;
    Transaction revertedTransaction;
    Transaction revertingTransaction;

    @BeforeEach
    @Transactional
    public void setupDb() {
        transactions = new ArrayList<>(
                List.of(fakeTransaction(faker, testHousehold.id), fakeTransaction(faker, testHousehold.id),
                        fakeTransaction(faker, testHousehold.id)));
        transactions.forEach(transaction -> {
            transaction.persist();
            transaction.sides.forEach(side -> {
                side.transaction = transaction;
                side.persist();
            });
        });
        revertedTransaction = transactions.get(2);
        revertingTransaction = fakeTransaction(faker, testHousehold.id);
        revertingTransaction.reverts = revertedTransaction;
        revertingTransaction.persist();
        revertingTransaction.sides.forEach(side -> {
            side.transaction = revertingTransaction;
            side.persist();
        });
        transactions.add(revertingTransaction);
    }

    @AfterEach
    @Transactional
    public void cleanDb() {
        TransactionSide.deleteAll();
        Transaction.deleteAll();
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void listTransactionsOk() {
        given().get()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("count", equalTo(transactions.size()))
                .body("results.id",
                        containsInAnyOrder(transactions.stream().map(transaction -> (int) transaction.id).toArray()))
                .body("results.name",
                        containsInAnyOrder(transactions.stream().map(transaction -> transaction.name).toArray()));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void listTransactionsPagedOk() {
        given().queryParam("pageSize", 10)
                .queryParam("pageIndex", 100)
                .get()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("count", equalTo(transactions.size()))
                .body("results", empty())
                .body("pageSize", equalTo(10))
                .body("pageIndex", equalTo(100));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void createTransactionOk() {
        TransactionCreateDto transactionCreateDto = fakeTransactionCreateDto(faker);
        int id = given().contentType(ContentType.JSON)
                .body(transactionCreateDto)
                .post()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(201)
                .body("name", equalTo(transactionCreateDto.name))
                .body("payers.userId",
                        contains(transactionCreateDto.payers.stream().map(payer -> payer.userId).toArray()))
                .body("beneficiaries.userId", contains(
                        transactionCreateDto.beneficiaries.stream().map(beneficiary -> beneficiary.userId).toArray()))
                .extract()
                .body()
                .path("id");

        Transaction transaction = Transaction.findById((long) id);
        assertThat(transaction.name, equalTo(transactionCreateDto.name));
        assertThat(transaction.amount, equalTo(transactionCreateDto.amount));
        assertThat(transaction.sides.size(),
                equalTo(transactionCreateDto.payers.size() + transactionCreateDto.beneficiaries.size()));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void createTransactionInvalid() {
        TransactionCreateDto transactionCreateDto = fakeTransactionCreateDto(faker);
        transactionCreateDto.beneficiaries =
                List.of(fakeTransactionSideCreateDto(faker), fakeTransactionSideCreateDto(faker));
        given().contentType(ContentType.JSON)
                .body(transactionCreateDto)
                .post()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getTransactionOk() {
        given().pathParam("transactionId", transactions.get(0).id)
                .get("/{transactionId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) transactions.get(0).id))
                .body("name", equalTo(transactions.get(0).name));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getTransactionNotFound() {
        given().get("/{transactionId}", -1).then().log().ifValidationFails().statusCode(404);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getTransactionHouseholdMismatch() {
        RestAssured.given()
                .pathParam("householdId", "not-found")
                .pathParam("transactionId", transactions.get(0).id)
                .get("/{transactionId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(404);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void updateTransactionOk() {
        TransactionUpdateDto transactionUpdateDto = new TransactionUpdateDto();
        transactionUpdateDto.name = faker.commerce().productName();
        given().contentType(ContentType.JSON)
                .body(transactionUpdateDto)
                .pathParam("transactionId", transactions.get(0).id)
                .patch("/{transactionId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) transactions.get(0).id))
                .body("name", equalTo(transactionUpdateDto.name))
                .body("comment", equalTo(transactions.get(0).comment));

        Transaction transaction = Transaction.findById(transactions.get(0).id);
        assertThat(transaction.name, equalTo(transactionUpdateDto.name));
        assertThat(transaction.comment, equalTo(transactions.get(0).comment));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void updateTransactionReverted() {
        given().contentType(ContentType.JSON)
                .body(new TransactionUpdateDto())
                .pathParam("transactionId", revertedTransaction.id)
                .patch("/{transactionId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void updateTransactionReverting() {
        given().contentType(ContentType.JSON)
                .body(new TransactionUpdateDto())
                .pathParam("transactionId", revertingTransaction.id)
                .patch("/{transactionId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void revertTransactionOk() {
        int id = given().pathParam("transactionId", transactions.get(0).id)
                .delete("/{transactionId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("name", containsStringIgnoringCase("revert"))
                .body("name", containsString(transactions.get(0).name))
                .body("payers.userId", containsInAnyOrder(transactions.get(0).sides.stream()
                        .filter(side -> side.type == TransactionSideType.BENEFICIARY)
                        .map(side -> side.userId)
                        .toArray()))
                .body("beneficiaries.userId", containsInAnyOrder(transactions.get(0).sides.stream()
                        .filter(side -> side.type == TransactionSideType.PAYER)
                        .map(side -> side.userId)
                        .toArray()))
                .extract()
                .body()
                .path("id");

        Transaction transaction = Transaction.findById((long) id);
        assertThat(transaction.sides.size(), equalTo(transactions.get(0).sides.size()));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void revertTransactionReverted() {
        given().pathParam("transactionId", revertedTransaction.id)
                .delete("/{transactionId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void revertTransactionReverting() {
        given().pathParam("transactionId", revertingTransaction.id)
                .delete("/{transactionId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);
    }
}
