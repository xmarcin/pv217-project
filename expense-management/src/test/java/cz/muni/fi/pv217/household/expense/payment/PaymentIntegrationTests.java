package cz.muni.fi.pv217.household.expense.payment;

import static cz.muni.fi.pv217.household.expense.payment.PaymentUtils.fakePayment;
import static cz.muni.fi.pv217.household.expense.payment.PaymentUtils.fakePaymentCreateDto;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

import cz.muni.fi.pv217.household.expense.TestsBase;
import cz.muni.fi.pv217.household.expense.payment.dtos.PaymentCreateDto;
import cz.muni.fi.pv217.household.expense.payment.models.Payment;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.ClaimType;
import io.quarkus.test.security.jwt.JwtSecurity;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import jakarta.transaction.Transactional;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestHTTPEndpoint(PaymentResource.class)
public class PaymentIntegrationTests extends TestsBase {

    List<Payment> payments;

    @BeforeEach
    @Transactional
    public void setupDb() {
        payments = List.of(fakePayment(faker, testHousehold.id), fakePayment(faker, testHousehold.id),
                fakePayment(faker, testHousehold.id));
        payments.forEach(payment -> payment.persist());
    }

    @AfterEach
    @Transactional
    public void cleanDb() {
        Payment.deleteAll();
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void listPaymentsOk() {
        given().queryParam("all", true)
                .get()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", containsInAnyOrder(payments.stream().map(payment -> (int) payment.id).toArray()))
                .body("name", containsInAnyOrder(payments.stream().map(payment -> payment.name).toArray()));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void createPaymentOk() {
        PaymentCreateDto paymentCreateDto = fakePaymentCreateDto(faker);
        int id = given().contentType(ContentType.JSON)
                .body(paymentCreateDto)
                .post()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(201)
                .body("name", equalTo(paymentCreateDto.name))
                .extract()
                .body()
                .path("id");

        Payment payment = Payment.findById(id);
        assertThat(payment.name, equalTo(paymentCreateDto.name));
        assertThat(payment.amount, equalTo(paymentCreateDto.amount));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void createPaymentInvalid() {
        PaymentCreateDto paymentCreateDto = fakePaymentCreateDto(faker);
        paymentCreateDto.amount = 0;
        given().contentType(ContentType.JSON)
                .body(paymentCreateDto)
                .post()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getPaymentOk() {
        given().pathParam("paymentId", payments.get(0).id)
                .get("/{paymentId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) payments.get(0).id))
                .body("name", equalTo(payments.get(0).name));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getPaymentNotFound() {
        given().pathParam("paymentId", -1).get("/{paymentId}").then().log().ifValidationFails().statusCode(404);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getPaymentHouseholdMismatch() {
        RestAssured.given()
                .pathParam("householdId", "not-found")
                .pathParam("paymentId", payments.get(0).id)
                .get("/{paymentId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(404);
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void updatePaymentOk() {
        PaymentCreateDto paymentCreateDto = fakePaymentCreateDto(faker);
        given().contentType(ContentType.JSON)
                .body(paymentCreateDto)
                .pathParam("paymentId", payments.get(0).id)
                .put("/{paymentId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) payments.get(0).id))
                .body("name", equalTo(paymentCreateDto.name));

        Payment payment = Payment.findById(payments.get(0).id);
        assertThat(payment.name, equalTo(paymentCreateDto.name));
        assertThat(payment.amount, equalTo(paymentCreateDto.amount));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void updatePaymentInvalid() {
        PaymentCreateDto paymentCreateDto = fakePaymentCreateDto(faker);
        paymentCreateDto.amount = 0;
        given().contentType(ContentType.JSON)
                .body(paymentCreateDto)
                .pathParam("paymentId", payments.get(0).id)
                .put("/{paymentId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(400);

        Payment payment = Payment.findById(payments.get(0).id);
        assertThat(payment.name, equalTo(payments.get(0).name));
        assertThat(payment.amount, equalTo(payments.get(0).amount));
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void deletePaymentOk() {
        given().pathParam("paymentId", payments.get(0).id)
                .delete("/{paymentId}")
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .body("id", equalTo((int) payments.get(0).id))
                .body("name", equalTo(payments.get(0).name));

        Payment payment = Payment.findById(payments.get(0).id);
        assertThat(payment.validUntil, lessThan(payment.validFrom));
    }
}
