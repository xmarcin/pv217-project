package cz.muni.fi.pv217.household.expense.payment;

import com.github.javafaker.Faker;
import cz.muni.fi.pv217.household.expense.payment.dtos.PaymentCreateDto;
import cz.muni.fi.pv217.household.expense.payment.dtos.PeriodUnit;
import cz.muni.fi.pv217.household.expense.payment.models.Payment;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

public class PaymentUtils {

    public static Payment fakePayment(Faker faker, String householdId) {
        Payment payment = new Payment();
        payment.name = faker.commerce().productName();
        payment.amount = faker.number().randomDouble(2, 1, 1000);
        payment.householdId = householdId != null ? householdId : faker.internet().uuid();
        payment.userId = faker.internet().uuid();
        payment.validFrom =
                faker.date().past(30, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        payment.validUntil =
                faker.date().future(30, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        payment.period = faker.number().numberBetween(1, 3);
        payment.periodUnit = PeriodUnit.MONTH;
        return payment;
    }

    public static PaymentCreateDto fakePaymentCreateDto(Faker faker) {
        PaymentCreateDto paymentCreateDto = new PaymentCreateDto();
        paymentCreateDto.name = faker.commerce().productName();
        paymentCreateDto.amount = faker.number().randomDouble(2, 1, 1000);
        paymentCreateDto.userId = "user" + faker.number().numberBetween(0, 4);
        paymentCreateDto.validFrom =
                faker.date().past(30, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        paymentCreateDto.validUntil =
                faker.date().future(30, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        paymentCreateDto.period = faker.number().numberBetween(1, 3);
        paymentCreateDto.periodUnit = PeriodUnit.MONTH;
        return paymentCreateDto;
    }
}
