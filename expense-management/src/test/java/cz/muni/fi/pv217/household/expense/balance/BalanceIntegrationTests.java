package cz.muni.fi.pv217.household.expense.balance;

import static cz.muni.fi.pv217.household.expense.payment.PaymentUtils.fakePayment;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

import cz.muni.fi.pv217.household.expense.TestsBase;
import cz.muni.fi.pv217.household.expense.payment.dtos.PeriodUnit;
import cz.muni.fi.pv217.household.expense.payment.models.Payment;
import cz.muni.fi.pv217.household.expense.transaction.models.Transaction;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSide;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSideType;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.ClaimType;
import io.quarkus.test.security.jwt.JwtSecurity;
import jakarta.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests for the balance endpoint.
 */
@QuarkusTest
@TestHTTPEndpoint(BalanceResource.class)
public class BalanceIntegrationTests extends TestsBase {

    List<String> users = List.of("user0", "user1", "user2", "user3", "user4");

    List<Payment> payments;

    List<Transaction> transactions;

    private List<String> userIds(Integer... idx) {
        return Arrays.stream(idx).map(users::get).toList();
    }

    private void addTransaction(String name, double amount, List<String> payers, List<String> beneficiaries,
                                boolean isPayment) {
        Transaction transaction = new Transaction();
        transaction.name = name;
        transaction.amount = amount;
        transaction.comment = "";
        transaction.householdId = testHousehold.id;
        transaction.authorId = "author";
        if (isPayment) {
            transaction.payment =
                    payments.stream().filter(payment -> payers.contains(payment.userId)).findFirst().orElse(null);
        }
        transaction.persist();
        transaction.sides = Stream.concat(payers.stream().map(userId -> {
            TransactionSide payerSide = new TransactionSide();
            payerSide.transaction = transaction;
            payerSide.userId = userId;
            payerSide.type = TransactionSideType.PAYER;
            payerSide.proportion = 1.0 / payers.size();
            payerSide.persist();
            return payerSide;
        }), beneficiaries.stream().map(userId -> {
            TransactionSide payerSide = new TransactionSide();
            payerSide.transaction = transaction;
            payerSide.userId = userId;
            payerSide.type = TransactionSideType.BENEFICIARY;
            payerSide.proportion = 1.0 / beneficiaries.size();
            payerSide.persist();
            return payerSide;
        })).toList();
        transactions.add(transaction);
    }

    @BeforeEach
    @Transactional
    public void setupDb() {
        transactions = new ArrayList<>();
        payments = users.stream().map(userId -> {
            Payment payment = fakePayment(faker, testHousehold.id);
            payment.userId = userId;
            payment.validFrom = LocalDate.of(2021, 1, 1);
            payment.validUntil = LocalDate.of(2021, 3, 1);
            payment.amount = 100.0;
            payment.period = 1;
            payment.periodUnit = PeriodUnit.MONTH;
            payment.persist();
            return payment;
        }).toList();

        // Payment for January, users 0, 2, 3, 4
        userIds(0, 2, 3, 4).forEach(
                userId -> addTransaction("Payment JAN", 100.0, List.of(userId), Collections.emptyList(), true));

        // Payment for February, users 0 and 2
        userIds(0, 2).forEach(
                userId -> addTransaction("Payment FEB", 100.0, List.of(userId), Collections.emptyList(), true));

        // Item 1, used by 0, 2, 3, 4, paid by 0
        addTransaction("Item 1", 200, userIds(0), userIds(0, 2, 3, 4), false);

        // Item 2, used by 0, 1, 3, 4, paid by 2, 3
        addTransaction("Item 2", 150, userIds(2, 3), users, false);

        // Refund Item 1, paid by 0
        addTransaction("Ref Item 1", 200, Collections.emptyList(), userIds(0), false);
    }

    @AfterEach
    @Transactional
    public void cleanDb() {
        TransactionSide.deleteAll();
        Transaction.deleteAll();
        Payment.deleteAll();
    }

    @Test
    @TestSecurity(user = "user0")
    @JwtSecurity(claims = @Claim(key = "sub", value = "user0", type = ClaimType.STRING))
    public void getBalance() {
        Balance balance =
                given().get("").then().log().ifValidationFails().statusCode(200).extract().body().as(Balance.class);

        Map<Integer, List<UserBalance>> userBalances = balance.userBalances.stream()
                .collect(Collectors.groupingBy(userBalance -> users.indexOf(userBalance.userId)));

        assertThat(balance.totalBalance, equalTo(400.0));
        assertThat(balance.totalPaymentBalance, equalTo(400.0));

        assertThat(userBalances.get(0).get(0).balance, equalTo(120.0));
        assertThat(userBalances.get(1).get(0).balance, equalTo(-30.0));
        assertThat(userBalances.get(2).get(0).balance, equalTo(195.0));
        assertThat(userBalances.get(3).get(0).balance, equalTo(95.0));
        assertThat(userBalances.get(4).get(0).balance, equalTo(20.0));
        assertThat(userBalances.get(0)
                .get(0).paymentBalances.stream()
                .map(userPaymentBalance -> userPaymentBalance.balance)
                .toList(), contains(0.0));
        assertThat(userBalances.get(1)
                .get(0).paymentBalances.stream()
                .map(userPaymentBalance -> userPaymentBalance.balance)
                .toList(), contains(200.0));
        assertThat(userBalances.get(2)
                .get(0).paymentBalances.stream()
                .map(userPaymentBalance -> userPaymentBalance.balance)
                .toList(), contains(0.0));
        assertThat(userBalances.get(3)
                .get(0).paymentBalances.stream()
                .map(userPaymentBalance -> userPaymentBalance.balance)
                .toList(), contains(100.0));
        assertThat(userBalances.get(4)
                .get(0).paymentBalances.stream()
                .map(userPaymentBalance -> userPaymentBalance.balance)
                .toList(), contains(100.0));
    }
}
