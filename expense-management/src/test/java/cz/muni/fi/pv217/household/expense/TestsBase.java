package cz.muni.fi.pv217.household.expense;

import static org.mockito.Mockito.when;

import com.github.javafaker.Faker;
import cz.muni.fi.pv217.household.expense.household.HouseholdApi;
import cz.muni.fi.pv217.household.expense.household.HouseholdDto;
import io.quarkus.test.InjectMock;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import java.util.Set;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeEach;

public abstract class TestsBase {
    @InjectMock
    @RestClient
    HouseholdApi householdApi;

    public Faker faker = new Faker();

    public HouseholdDto testHousehold = TestsBase.fakeHouseholdDto();

    public RequestSpecification given() {
        return RestAssured.given().pathParam("householdId", testHousehold.id);
    }

    private static HouseholdDto fakeHouseholdDto() {
        Faker faker = new Faker();
        HouseholdDto householdDto = new HouseholdDto();
        householdDto.id = faker.internet().uuid();
        householdDto.name = faker.commerce().productName();
        householdDto.ownerId = "user0";
        householdDto.memberIds = Set.of("user0", "user1", "user2", "user3", "user4");
        householdDto.address = faker.address().fullAddress();
        householdDto.accountNumber = faker.finance().iban();
        return householdDto;
    }

    @BeforeEach
    public void setupMockApi() {
        when(householdApi.getHousehold(testHousehold.id)).thenReturn(testHousehold);
    }
}
