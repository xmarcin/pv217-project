package cz.muni.fi.pv217.household.expense.balance;

import cz.muni.fi.pv217.household.expense.household.HouseholdApi;
import cz.muni.fi.pv217.household.expense.household.HouseholdDto;
import io.quarkus.security.Authenticated;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import java.util.Objects;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.rest.client.inject.RestClient;

/**
 * Class for handling balance endpoint.
 */
@Path("/{householdId}/balance")
@Authenticated
@RequestScoped
public class BalanceResource {

    @Inject
    BalanceService balanceService;

    @PathParam("householdId")
    private String householdId;

    @Inject
    @Claim(standard = Claims.sub)
    String userId;

    @RestClient
    HouseholdApi householdApi;

    /**
     * Lists user balances in the household.
     *
     * @return list of balances
     */
    @GET
    public Balance index() {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        Balance balance = balanceService.calculateBalance(householdId);
        if (!isOwner) {
            balance.userBalances =
                    balance.userBalances.stream().filter(userBalance -> userBalance.userId.equals(userId)).toList();
        }
        return balance;
    }
}
