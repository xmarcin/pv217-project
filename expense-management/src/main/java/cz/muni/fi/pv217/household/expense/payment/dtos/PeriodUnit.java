package cz.muni.fi.pv217.household.expense.payment.dtos;

import java.time.Period;

/**
 * Enum representing payment period unit.
 */
public enum PeriodUnit {
    DAY, WEEK, MONTH, YEAR;

    /**
     * Creates a new Period from this unit with period.
     *
     * @param period period
     * @return Period
     */
    public Period of(int period) {
        return switch (this) {
            case DAY -> Period.ofDays(period);
            case WEEK -> Period.ofWeeks(period);
            case MONTH -> Period.ofMonths(period);
            case YEAR -> Period.ofYears(period);
        };
    }
}
