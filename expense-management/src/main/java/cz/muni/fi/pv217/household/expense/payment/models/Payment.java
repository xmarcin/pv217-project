package cz.muni.fi.pv217.household.expense.payment.models;

import cz.muni.fi.pv217.household.expense.common.models.BaseEntity;
import cz.muni.fi.pv217.household.expense.payment.dtos.PeriodUnit;
import cz.muni.fi.pv217.household.expense.transaction.models.Transaction;
import jakarta.annotation.Nullable;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.List;

/**
 * Class representing Recurring entity.
 */
@Entity
public class Payment extends BaseEntity {

    @NotBlank
    public String name;

    @Positive
    public double amount;

    @NotNull
    public String householdId;

    @NotNull
    public String userId;

    @NotNull
    public LocalDate validFrom;

    @Nullable
    public LocalDate validUntil;

    @Positive
    public int period;

    @NotNull
    public PeriodUnit periodUnit;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "payment")
    public List<Transaction> transactions;
}
