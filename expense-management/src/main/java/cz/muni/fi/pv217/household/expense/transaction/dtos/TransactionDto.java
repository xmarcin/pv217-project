package cz.muni.fi.pv217.household.expense.transaction.dtos;

import cz.muni.fi.pv217.household.expense.common.dtos.BaseDto;
import cz.muni.fi.pv217.household.expense.payment.dtos.PaymentDto;
import cz.muni.fi.pv217.household.expense.transaction.validation.TransactionSides;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import java.util.List;

/**
 * Class for transfer of Transaction data.
 */
public class TransactionDto extends BaseDto {

    @NotBlank
    public String name;

    @NotNull
    public String comment;

    @Positive
    public double amount;

    @NotNull
    public String householdId;

    @NotNull
    public String authorId;

    @Nullable
    public TransactionDto revertedBy;

    @Size(min = 1)
    @TransactionSides
    public List<TransactionSideDto> payers;

    @Size(min = 1)
    @TransactionSides
    public List<TransactionSideDto> beneficiaries;

    public PaymentDto payment;
}
