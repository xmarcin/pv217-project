package cz.muni.fi.pv217.household.expense;

import cz.muni.fi.pv217.household.expense.common.dtos.ErrorResponse;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.util.UUID;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

/**
 * Class for mapping exceptions to responses.
 */
public class ExceptionMapper {

    public static final Logger LOGGER = Logger.getLogger(ExceptionMapper.class);

    /**
     * Maps Web app exceptions to an error response.
     *
     * @param exception thrown exception
     * @return response
     */
    @ServerExceptionMapper
    public Response mapWebAppException(WebApplicationException exception) {
        UUID errorId = UUID.randomUUID();
        Response response = exception.getResponse();
        if (response.hasEntity()) {
            return response;
        }

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.errorId = errorId;
        errorResponse.status = response.getStatus();
        if (errorResponse.status % 100 == 5) {
            LOGGER.errorf(exception, "Unexpected exception (%s)", errorId);
            errorResponse.title = "Unexpected server exception";
        } else {
            errorResponse.title = exception.getMessage();
        }
        return RestResponse.status(response.getStatusInfo(), errorResponse).toResponse();
    }
}
