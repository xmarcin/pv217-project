package cz.muni.fi.pv217.household.expense.balance;

import cz.muni.fi.pv217.household.expense.payment.PaymentMapper;
import cz.muni.fi.pv217.household.expense.payment.models.Payment;
import cz.muni.fi.pv217.household.expense.transaction.models.Transaction;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSideType;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class handling balance calculations.
 */
@RequestScoped
public class BalanceService {

    @Inject
    PaymentMapper paymentMapper;

    private UserPaymentBalance calculatePaymentBalance(Payment payment) {
        UserPaymentBalance paymentBalance = new UserPaymentBalance();
        paymentBalance.payment = paymentMapper.toResource(payment);
        // TODO: init from cache
        paymentBalance.balance = 0;
        LocalDate from = payment.validFrom;
        LocalDate until = payment.validUntil.isBefore(LocalDate.now()) ? payment.validUntil : LocalDate.now();

        paymentBalance.balance +=
                payment.amount * from.datesUntil(until, payment.periodUnit.of(payment.period)).count();
        // TODO: save to cache

        return paymentBalance;
    }

    private Map<String, List<UserPaymentBalance>> calculateUserPaymentBalances(String householdId) {
        List<Payment> payments = Payment.find("householdId", householdId).list();
        return payments.stream()
                .map(this::calculatePaymentBalance)
                .collect(Collectors.groupingBy(paymentBalance -> paymentBalance.payment.userId));
    }

    private List<UserBalance> calculateUserBalances(String householdId) {
        // TODO: init from cache
        Map<String, WorkingUserBalance> workingBalances = new HashMap<>();

        // TODO: determine cache point
        // Summarize transactions
        Transaction.find("householdId", Transaction.createdAtDesc(), householdId).list().forEach(e -> {
            Transaction transaction = (Transaction) e;
            transaction.sides.forEach(side -> {
                WorkingUserBalance balance =
                        workingBalances.computeIfAbsent(side.userId, s -> new WorkingUserBalance());
                double sideAmount = transaction.amount * side.proportion;
                if (side.type == TransactionSideType.BENEFICIARY) {
                    sideAmount *= -1.0;
                }
                balance.transactionBalance += sideAmount;
                if (transaction.payment != null) {
                    balance.paymentBalances.put(transaction.payment.id,
                            balance.paymentBalances.getOrDefault(transaction.payment.id, 0.0) + sideAmount);
                }
            });
        });

        // Add payments
        Map<String, List<UserPaymentBalance>> userPaymentBalances = calculateUserPaymentBalances(householdId);
        return workingBalances.keySet()
                .stream()
                .map(userId -> workingBalances.get(userId)
                        .finalize(userId, userPaymentBalances.getOrDefault(userId, Collections.emptyList())))
                .toList();
    }

    /**
     * Calculates balance for each user and the household as whole..
     *
     * @param householdId household id
     * @return calculated balances
     */
    public Balance calculateBalance(String householdId) {
        Balance balance = new Balance();
        balance.userBalances = calculateUserBalances(householdId);
        balance.totalBalance = balance.userBalances.stream().mapToDouble(userBalance -> userBalance.balance).sum();
        balance.totalPaymentBalance = balance.userBalances.stream()
                .mapToDouble(userBalance -> userBalance.paymentBalances.stream()
                        .mapToDouble(userPaymentBalance -> userPaymentBalance.balance)
                        .sum())
                .sum();
        return balance;
    }

    private static class WorkingUserBalance {
        public double transactionBalance = 0;

        public Map<Long, Double> paymentBalances = new HashMap<>();

        public UserBalance finalize(String userId, List<UserPaymentBalance> userPaymentBalances) {
            UserBalance userBalance = new UserBalance();
            userBalance.userId = userId;
            userBalance.balance = transactionBalance;
            userBalance.paymentBalances = userPaymentBalances.stream()
                    .peek(paymentBalance -> paymentBalance.balance -=
                            paymentBalances.getOrDefault(paymentBalance.payment.id, 0.0))
                    .toList();
            return userBalance;
        }
    }
}
