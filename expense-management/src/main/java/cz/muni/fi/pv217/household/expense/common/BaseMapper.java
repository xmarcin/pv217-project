package cz.muni.fi.pv217.household.expense.common;

import cz.muni.fi.pv217.household.expense.common.dtos.PagedResultDto;
import cz.muni.fi.pv217.household.expense.common.models.PagedResult;
import java.util.List;
import org.mapstruct.Mapping;

/**
 * Base mapper.
 *
 * @param <DtoT>   DTO class type
 * @param <ModelT> model class type
 */
public interface BaseMapper<DtoT, ModelT> {

    DtoT toResource(ModelT transaction);

    List<DtoT> toResources(List<ModelT> transactions);

    @Mapping(target = "results", expression = "java(toResources(pagedResult.results))")
    PagedResultDto<DtoT> toPagedResources(PagedResult<ModelT> pagedResult);
}
