package cz.muni.fi.pv217.household.expense.balance;

import java.util.List;

/**
 * Class for transport of balance.
 */
public class Balance {

    public double totalBalance;

    public double totalPaymentBalance;

    public List<UserBalance> userBalances;
}
