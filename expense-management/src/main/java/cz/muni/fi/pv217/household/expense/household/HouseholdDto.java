package cz.muni.fi.pv217.household.expense.household;

import java.util.Set;

/**
 * Household data object.
 */
public class HouseholdDto {
    public String id;

    public String name;

    public String accountNumber;

    public String address;

    public String ownerId;

    public Set<String> memberIds;
}
