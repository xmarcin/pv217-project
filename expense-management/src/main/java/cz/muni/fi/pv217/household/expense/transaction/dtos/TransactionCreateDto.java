package cz.muni.fi.pv217.household.expense.transaction.dtos;

import cz.muni.fi.pv217.household.expense.transaction.validation.TransactionSides;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import java.util.List;

/**
 * Class for transfer of Transaction Create data.
 */
public class TransactionCreateDto {

    @NotBlank
    public String name;

    @NotNull
    public String comment;

    @Positive
    public double amount;

    @Size(min = 1)
    @TransactionSides
    public List<TransactionSideCreateDto> payers;

    @Size(min = 1)
    @TransactionSides
    public List<TransactionSideCreateDto> beneficiaries;

    public long paymentId;
}
