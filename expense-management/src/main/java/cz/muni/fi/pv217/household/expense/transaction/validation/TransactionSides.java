package cz.muni.fi.pv217.household.expense.transaction.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Class for validating Transaction Sides.
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TransactionSidesValidator.class)
@Documented
public @interface TransactionSides {
    /**
     * Validation error message.
     *
     * @return message
     */
    String message() default "Transaction sides are not valid";

    /**
     * Validation groups.
     *
     * @return groups
     */
    Class<?>[] groups() default {};

    /**
     * Validation payload.
     *
     * @return payload
     */
    Class<? extends Payload>[] payload() default {};
}
