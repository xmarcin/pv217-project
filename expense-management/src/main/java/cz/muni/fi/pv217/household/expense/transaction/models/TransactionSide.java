package cz.muni.fi.pv217.household.expense.transaction.models;

import cz.muni.fi.pv217.household.expense.common.models.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

/**
 * Class representing Transaction Side model.
 */
@Entity
public class TransactionSide extends BaseEntity {

    @NotNull
    public String userId;

    @NotNull
    @Min(0)
    @Max(1)
    public double proportion;

    @NotNull
    public TransactionSideType type;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    public Transaction transaction;

}
