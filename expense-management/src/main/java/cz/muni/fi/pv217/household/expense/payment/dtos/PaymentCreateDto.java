package cz.muni.fi.pv217.household.expense.payment.dtos;

import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import java.time.LocalDate;

/**
 * Class representing Recurring Transaction entity.
 */
public class PaymentCreateDto {

    @NotBlank
    public String name;

    @Positive
    public double amount;

    @NotNull
    public String userId;

    @NotNull
    public LocalDate validFrom;

    @Nullable
    public LocalDate validUntil;

    @Positive
    public int period;

    @NotNull
    public PeriodUnit periodUnit;
}
