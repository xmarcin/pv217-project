package cz.muni.fi.pv217.household.expense.payment;

import cz.muni.fi.pv217.household.expense.household.HouseholdApi;
import cz.muni.fi.pv217.household.expense.household.HouseholdDto;
import cz.muni.fi.pv217.household.expense.payment.dtos.PaymentCreateDto;
import cz.muni.fi.pv217.household.expense.payment.dtos.PaymentDto;
import io.quarkus.security.Authenticated;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import java.util.List;
import java.util.Objects;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestQuery;

/**
 * Class handling Payment REST Resource.
 */
@Path("/{householdId}/payments")
@Authenticated
@RequestScoped
public class PaymentResource {

    @Inject
    PaymentMapper paymentMapper;

    @Inject
    PaymentService paymentService;

    @PathParam("householdId")
    private String householdId;

    @Inject
    @Claim(standard = Claims.sub)
    String userId;

    @RestClient
    HouseholdApi householdApi;

    /**
     * Lists payments in a household.
     *
     * @param all  list all payments instead of current user ones
     * @param past include payments after their validUntil date
     * @return payments list
     */
    @GET
    public List<PaymentDto> index(@RestQuery boolean all, @RestQuery boolean past) {
        householdApi.getHousehold(householdId);
        return paymentMapper.toResources(paymentService.listPayments(householdId, all ? null : userId, past));
    }

    /**
     * Creates a new payment in a household.
     *
     * @param paymentCreateDto payment create data
     * @return created payment
     */
    @POST
    @ResponseStatus(201)
    public PaymentDto create(@Valid PaymentCreateDto paymentCreateDto) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        if (!isOwner) {
            throw new BadRequestException("Only owner can add payments");
        }
        if (!householdDto.memberIds.contains(paymentCreateDto.userId)) {
            throw new BadRequestException("User is not part of the household");
        }
        return paymentMapper.toResource(paymentService.createPayment(householdId, paymentCreateDto));
    }

    /**
     * Finds a payment in a household.
     *
     * @param paymentId payment id
     * @return payment
     */
    @GET
    @Path("/{paymentId}")
    public PaymentDto read(@RestPath long paymentId) {
        householdApi.getHousehold(householdId);
        return paymentMapper.toResource(paymentService.findPayment(householdId, paymentId));
    }

    /**
     * Updates a payment in a household.
     *
     * @param paymentId        payment id
     * @param paymentCreateDto updated payment data
     * @return updated payment
     */
    @PUT
    @Path("/{paymentId}")
    public PaymentDto update(@RestPath long paymentId, @Valid PaymentCreateDto paymentCreateDto) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        if (!isOwner) {
            throw new BadRequestException("Only owner can update payments");
        }
        if (!householdDto.memberIds.contains(paymentCreateDto.userId)) {
            throw new BadRequestException("User is not part of the household");
        }
        return paymentMapper.toResource(paymentService.updatePayment(householdId, paymentId, paymentCreateDto));
    }

    /**
     * Deletes a payment in a household.
     *
     * @param paymentId payment id
     * @return deleted payment
     */
    @DELETE
    @Path("/{paymentId}")
    public PaymentDto delete(@RestPath long paymentId) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        if (!isOwner) {
            throw new BadRequestException("Only owner can add payments");
        }
        return paymentMapper.toResource(paymentService.deletePayment(householdId, paymentId));
    }
}
