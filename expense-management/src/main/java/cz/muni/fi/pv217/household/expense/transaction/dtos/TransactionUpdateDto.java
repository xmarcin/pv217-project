package cz.muni.fi.pv217.household.expense.transaction.dtos;

/**
 * Class for transfer of Transaction Update data.
 */
public class TransactionUpdateDto {

    public String name;

    public String comment;
}
