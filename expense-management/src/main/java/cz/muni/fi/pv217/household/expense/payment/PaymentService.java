package cz.muni.fi.pv217.household.expense.payment;

import cz.muni.fi.pv217.household.expense.payment.dtos.PaymentCreateDto;
import cz.muni.fi.pv217.household.expense.payment.models.Payment;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Class handling Payments.
 */
@RequestScoped
public class PaymentService {

    /**
     * Lists payments in a household.
     *
     * @param householdId household id
     * @param userId      optionally filter by userId
     * @param past        include payments after their validUntil date
     * @return payments list
     */
    public List<Payment> listPayments(String householdId, String userId, boolean past) {
        StringBuilder queryBuilder = new StringBuilder("householdId = :householdId");
        Parameters queryParams = Parameters.with("householdId", householdId);
        if (userId != null) {
            queryBuilder.append(" and userId = :userId");
            queryParams.and("userId", userId);
        }
        if (!past) {
            queryBuilder.append(" and (validUntil = NULL or validUntil >= :now)");
            queryParams.and("now", LocalDate.now());
        }
        return Payment.list(queryBuilder.toString(), Payment.createdAtDesc(), queryParams);
    }

    private void setPaymentFields(Payment payment, PaymentCreateDto paymentCreateDto) {
        payment.name = paymentCreateDto.name;
        payment.amount = paymentCreateDto.amount;
        payment.userId = paymentCreateDto.userId;
        payment.validFrom = paymentCreateDto.validFrom;
        payment.validUntil = paymentCreateDto.validUntil;
        payment.period = paymentCreateDto.period;
        payment.periodUnit = paymentCreateDto.periodUnit;
    }

    /**
     * Creates a new payment.
     *
     * @param householdId      household id
     * @param paymentCreateDto payment create data
     * @return created payment
     */
    @Transactional
    public Payment createPayment(String householdId, PaymentCreateDto paymentCreateDto) {
        Payment payment = new Payment();
        payment.householdId = householdId;
        setPaymentFields(payment, paymentCreateDto);
        payment.persist();
        return payment;
    }

    /**
     * Finds a payment.
     *
     * @param householdId household id
     * @param paymentId   payment id
     * @return payment
     */
    public Payment findPayment(String householdId, long paymentId) {
        Payment payment = Payment.findById(paymentId);
        if (payment == null || !payment.householdId.equals(householdId)) {
            throw new NotFoundException("Payment not found");
        }
        return payment;
    }

    /**
     * Updates a payment.
     *
     * @param householdId      household id
     * @param paymentId        payment id
     * @param paymentCreateDto payment update data
     * @return updated payment
     */
    @Transactional
    public Payment updatePayment(String householdId, long paymentId, PaymentCreateDto paymentCreateDto) {
        Payment payment = findPayment(householdId, paymentId);
        setPaymentFields(payment, paymentCreateDto);
        return payment;
    }

    /**
     * Deletes a payment.
     *
     * @param householdId household id
     * @param paymentId   payment id
     */
    @Transactional
    public Payment deletePayment(String householdId, long paymentId) {
        Payment payment = findPayment(householdId, paymentId);
        payment.validUntil = payment.validFrom.minus(1, ChronoUnit.DAYS);
        return payment;
    }
}
