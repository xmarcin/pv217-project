package cz.muni.fi.pv217.household.expense.transaction.exceptions;

import jakarta.ws.rs.BadRequestException;

/**
 * Exception thrown when transaction cannot be reverted as it already is.
 */
public class TransactionAlreadyRevertedException extends BadRequestException {
    public TransactionAlreadyRevertedException() {
        super("Transaction is already reverted");
    }
}
