package cz.muni.fi.pv217.household.expense.transaction.dtos;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

/**
 * Class for transfer of Transaction Side Create data.
 */
public class TransactionSideCreateDto {

    @NotNull
    public String userId;
    @Positive
    @Max(1)
    public double proportion;
}
