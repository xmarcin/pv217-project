package cz.muni.fi.pv217.household.expense.transaction;

import cz.muni.fi.pv217.household.expense.common.dtos.PagedResultDto;
import cz.muni.fi.pv217.household.expense.household.HouseholdApi;
import cz.muni.fi.pv217.household.expense.household.HouseholdDto;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionCreateDto;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionDto;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionUpdateDto;
import io.quarkus.panache.common.Page;
import io.quarkus.security.Authenticated;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import java.util.Objects;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestQuery;

/**
 * Class handling Transaction REST Resource.
 */
@Path("/{householdId}/transactions")
@Authenticated
@RequestScoped
public class TransactionResource {

    @Inject
    TransactionService transactionService;

    @Inject
    TransactionMapper transactionMapper;

    @PathParam("householdId")
    private String householdId;

    @Inject
    @Claim(standard = Claims.sub)
    String userId;

    @RestClient
    HouseholdApi householdApi;

    /**
     * List all transactions.
     *
     * @param pageIndex index of the page
     * @param pageSize  page size
     * @return paged list of transactions
     */
    @GET
    public PagedResultDto<TransactionDto> index(@RestQuery @DefaultValue("0") int pageIndex,
                                                @RestQuery @DefaultValue("30") int pageSize) {
        householdApi.getHousehold(householdId);
        return transactionMapper.toPagedResources(
                transactionService.listTransactions(householdId, Page.of(pageIndex, pageSize)));
    }

    /**
     * Creates a new Transaction.
     *
     * @param transactionCreateDto transaction data
     * @return created transaction
     */
    @POST
    @ResponseStatus(201)
    public TransactionDto create(@Valid TransactionCreateDto transactionCreateDto) {
        HouseholdDto householdDto = householdApi.getHousehold(householdId);
        boolean isOwner = Objects.equals(householdDto.ownerId, userId);
        if (!isOwner) {
            throw new BadRequestException("Only owner can update payments");
        }
        if (!transactionCreateDto.payers.stream().allMatch(payer -> householdDto.memberIds.contains(payer.userId))) {
            throw new BadRequestException("Payer is not part of the household");
        }
        if (!transactionCreateDto.beneficiaries.stream()
                .allMatch(payer -> householdDto.memberIds.contains(payer.userId))) {
            throw new BadRequestException("Beneficiary is not part of the household");
        }
        return transactionMapper.toResource(
                transactionService.createTransaction(householdId, userId, transactionCreateDto));
    }

    /**
     * Finds a transaction in a household.
     *
     * @param transactionId transaction id
     * @return transaction
     */
    @GET
    @Path("/{transactionId}")
    public TransactionDto read(@RestPath long transactionId) {
        householdApi.getHousehold(householdId);
        return transactionMapper.toResource(transactionService.findTransaction(householdId, transactionId));
    }

    /**
     * Partially updates a transaction.
     *
     * @param transactionId        transaction id
     * @param transactionUpdateDto transaction update data
     * @return updated transaction
     */
    @PATCH
    @Path("/{transactionId}")
    public TransactionDto update(@RestPath long transactionId, @Valid TransactionUpdateDto transactionUpdateDto) {
        householdApi.getHousehold(householdId);
        return transactionMapper.toResource(
                transactionService.updateTransaction(householdId, transactionId, transactionUpdateDto));
    }

    /**
     * Reverts a transaction.
     *
     * @param transactionId transaction id
     * @return reverting transaction
     */
    @DELETE
    @Path("/{transactionId}")
    public TransactionDto delete(@RestPath long transactionId) {
        householdApi.getHousehold(householdId);
        return transactionMapper.toResource(transactionService.revertTransaction(householdId, transactionId, userId));
    }
}
