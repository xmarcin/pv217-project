package cz.muni.fi.pv217.household.expense.transaction.validation;

import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionSideCreateDto;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.List;

/**
 * Class for validating Transaction Sides.
 */
public class TransactionSidesValidator
        implements ConstraintValidator<TransactionSides, List<TransactionSideCreateDto>> {
    @Override
    public boolean isValid(List<TransactionSideCreateDto> value, ConstraintValidatorContext context) {
        return value.stream().mapToDouble(transactionSideCreateDto -> transactionSideCreateDto.proportion).sum() == 1;
    }
}
