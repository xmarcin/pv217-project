package cz.muni.fi.pv217.household.expense.transaction;

import cz.muni.fi.pv217.household.expense.common.models.PagedResult;
import cz.muni.fi.pv217.household.expense.payment.PaymentService;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionCreateDto;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionSideCreateDto;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionUpdateDto;
import cz.muni.fi.pv217.household.expense.transaction.exceptions.RevertingTransactionException;
import cz.muni.fi.pv217.household.expense.transaction.exceptions.TransactionAlreadyRevertedException;
import cz.muni.fi.pv217.household.expense.transaction.models.Transaction;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSide;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSideType;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import java.util.stream.Stream;

/**
 * Class handling Transactions.
 */
@RequestScoped
public class TransactionService {

    @Inject
    PaymentService paymentService;

    /**
     * Finds one page of transactions in household.
     *
     * @param householdId household uuid to find in
     * @param page        page to find
     * @return completed page
     */
    public PagedResult<Transaction> listTransactions(String householdId, Page page) {
        PanacheQuery<Transaction> query =
                Transaction.find("householdId", Transaction.createdAtDesc(), householdId).page(page);

        return new PagedResult<>(query, page);
    }

    private TransactionSide createTransactionSide(TransactionSideCreateDto sideCreateDto, TransactionSideType type,
                                                  Transaction transaction) {
        TransactionSide side = new TransactionSide();
        side.userId = sideCreateDto.userId;
        side.proportion = sideCreateDto.proportion;
        side.type = type;
        side.transaction = transaction;
        side.persist();
        return side;
    }

    /**
     * Creates a new transaction.
     *
     * @param householdId          householdId of the new transaction
     * @param authorId             authorId of the new transaction
     * @param transactionCreateDto transaction create data
     * @return persisted transaction
     */
    @Transactional
    public Transaction createTransaction(String householdId, String authorId,
                                         TransactionCreateDto transactionCreateDto) {
        Transaction transaction = new Transaction();
        transaction.name = transactionCreateDto.name;
        transaction.comment = transactionCreateDto.comment;
        transaction.amount = transactionCreateDto.amount;
        transaction.householdId = householdId;
        transaction.authorId = authorId;
        if (transactionCreateDto.paymentId != 0) {
            transaction.payment = paymentService.findPayment(householdId, transactionCreateDto.paymentId);
        }
        transaction.persist();
        transaction.sides = Stream.concat(transactionCreateDto.payers.stream()
                        .map(payer -> createTransactionSide(payer, TransactionSideType.PAYER, transaction)),
                transactionCreateDto.beneficiaries.stream()
                        .map(beneficiary -> createTransactionSide(beneficiary, TransactionSideType.BENEFICIARY,
                                transaction))).toList();


        return transaction;
    }

    /**
     * Finds a transaction in household.
     *
     * @param householdId   household id
     * @param transactionId transaction id
     * @return transaction if found
     */
    public Transaction findTransaction(String householdId, long transactionId) {
        Transaction transaction = Transaction.findById(transactionId);
        if (transaction == null || !transaction.householdId.equals(householdId)) {
            throw new NotFoundException("Transaction not found");
        }

        return transaction;
    }

    /**
     * Updates the transaction information.
     *
     * @param householdId          household id
     * @param transactionId        transaction id
     * @param transactionUpdateDto updated transaction information
     * @return updated transaction
     */
    @Transactional
    public Transaction updateTransaction(String householdId, long transactionId,
                                         TransactionUpdateDto transactionUpdateDto) {
        Transaction transaction = findTransaction(householdId, transactionId);

        if (transaction.revertedBy != null) {
            throw new TransactionAlreadyRevertedException();
        }
        if (transaction.reverts != null) {
            throw new RevertingTransactionException();
        }

        if (transactionUpdateDto.name != null && !transactionUpdateDto.name.isBlank()) {
            transaction.name = transactionUpdateDto.name;
        }
        if (transactionUpdateDto.comment != null && !transactionUpdateDto.comment.isBlank()) {
            transaction.comment = transactionUpdateDto.comment;
        }

        System.out.println(transaction.isPersistent());

        return transaction;
    }

    /**
     * Reverts a transaction.
     *
     * @param authorId      id of the user reverting the transaction
     * @param transactionId id of transaction to revert
     * @return reverting transaction
     */
    @Transactional
    public Transaction revertTransaction(String householdId, long transactionId, String authorId) {
        Transaction transaction = findTransaction(householdId, transactionId);

        if (transaction.revertedBy != null) {
            throw new TransactionAlreadyRevertedException();
        }
        if (transaction.reverts != null) {
            throw new RevertingTransactionException();
        }

        Transaction revertingTransaction = new Transaction();
        revertingTransaction.name = "Revert of \"%s\"".formatted(transaction.name);
        revertingTransaction.comment = transaction.comment;
        revertingTransaction.amount = transaction.amount;
        revertingTransaction.householdId = householdId;
        revertingTransaction.authorId = authorId;
        revertingTransaction.sides = transaction.sides.stream().map(side -> {
            TransactionSide sideCopy = new TransactionSide();
            sideCopy.userId = side.userId;
            sideCopy.proportion = side.proportion;
            sideCopy.type = side.type == TransactionSideType.PAYER ? TransactionSideType.BENEFICIARY
                    : TransactionSideType.PAYER;
            sideCopy.transaction = revertingTransaction;
            sideCopy.persist();
            return sideCopy;
        }).toList();
        revertingTransaction.reverts = transaction;
        revertingTransaction.payment = transaction.payment;
        revertingTransaction.persist();

        return revertingTransaction;
    }
}
