package cz.muni.fi.pv217.household.expense.common.dtos;

import java.util.UUID;

/**
 * Class representing error response body.
 */
public class ErrorResponse {
    public UUID errorId;
    public String title;
    public int status;
}
