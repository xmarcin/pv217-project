package cz.muni.fi.pv217.household.expense.transaction;

import cz.muni.fi.pv217.household.expense.common.BaseMapper;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionSideDto;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSide;
import org.mapstruct.Mapper;

/**
 * Class for mapping TransactionSide.
 */
@Mapper(componentModel = "jakarta")
public interface TransactionSideMapper extends BaseMapper<TransactionSideDto, TransactionSide> {
}
