package cz.muni.fi.pv217.household.expense.transaction.models;

/**
 * Enum representing transaction side.
 */
public enum TransactionSideType {
    PAYER, BENEFICIARY,
}
