package cz.muni.fi.pv217.household.expense.balance;

import java.util.List;

/**
 * Class for transport of User Balance.
 */
public class UserBalance {

    public String userId;

    public double balance;

    public List<UserPaymentBalance> paymentBalances;
}
