package cz.muni.fi.pv217.household.expense.transaction.exceptions;

import jakarta.ws.rs.BadRequestException;

/**
 * Transaction thrown when reverting transaction is not valid in a context.
 */
public class RevertingTransactionException extends BadRequestException {
    public RevertingTransactionException() {
        super("Transaction is reverting transaction");
    }
}
