package cz.muni.fi.pv217.household.expense.transaction.models;

import cz.muni.fi.pv217.household.expense.common.models.BaseEntity;
import cz.muni.fi.pv217.household.expense.payment.models.Payment;
import jakarta.annotation.Nullable;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import java.util.List;

/**
 * Class representing Transaction model.
 */
@Entity
public class Transaction extends BaseEntity {

    @NotBlank
    public String name;

    @NotNull
    public String comment;

    @NotNull
    @Positive
    public double amount;

    @NotNull
    public String householdId;

    @NotNull
    public String authorId;

    @OneToOne(fetch = FetchType.EAGER)
    @Nullable
    public Transaction reverts;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "reverts")
    @Nullable
    public Transaction revertedBy;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "transaction")
    @Size(min = 1)
    public List<TransactionSide> sides;

    @ManyToOne(fetch = FetchType.EAGER)
    @Nullable
    public Payment payment;
}
