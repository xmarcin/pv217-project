package cz.muni.fi.pv217.household.expense.balance;

import cz.muni.fi.pv217.household.expense.payment.dtos.PaymentDto;

/**
 * Class for transport of a Payment balance.
 */
public class UserPaymentBalance {

    public PaymentDto payment;

    public double balance;
}
