package cz.muni.fi.pv217.household.expense.transaction;

import cz.muni.fi.pv217.household.expense.common.BaseMapper;
import cz.muni.fi.pv217.household.expense.payment.PaymentMapper;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionDto;
import cz.muni.fi.pv217.household.expense.transaction.dtos.TransactionSideDto;
import cz.muni.fi.pv217.household.expense.transaction.models.Transaction;
import cz.muni.fi.pv217.household.expense.transaction.models.TransactionSideType;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * Interface for mapping Transactions.
 */
@Mapper(componentModel = "jakarta")
public interface TransactionMapper extends BaseMapper<TransactionDto, Transaction> {

    TransactionSideMapper sideMapper = Mappers.getMapper(TransactionSideMapper.class);
    PaymentMapper paymentMapper = Mappers.getMapper(PaymentMapper.class);

    /**
     * Filters payers from the transaction sides list.
     *
     * @param transaction transaction
     * @return payers list
     */
    default List<TransactionSideDto> payers(Transaction transaction) {
        return transaction.sides.stream()
                .filter(side -> side.type == TransactionSideType.PAYER)
                .map(sideMapper::toResource)
                .toList();
    }

    /**
     * Filters beneficiaries from the transaction sides list.
     *
     * @param transaction transaction
     * @return payers list
     */
    default List<TransactionSideDto> beneficiaries(Transaction transaction) {
        return transaction.sides.stream()
                .filter(side -> side.type == TransactionSideType.BENEFICIARY)
                .map(sideMapper::toResource)
                .toList();
    }

    @Mapping(target = "payers", expression = "java(payers(transaction))")
    @Mapping(target = "beneficiaries", expression = "java(beneficiaries(transaction))")
    @Mapping(target = "revertedBy", expression = "java(toResource(transaction.revertedBy))")
    @Mapping(target = "payment", expression = "java(paymentMapper.toResource(transaction.payment))")
    TransactionDto toResource(Transaction transaction);
}
