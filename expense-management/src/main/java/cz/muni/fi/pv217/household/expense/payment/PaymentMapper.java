package cz.muni.fi.pv217.household.expense.payment;

import cz.muni.fi.pv217.household.expense.common.BaseMapper;
import cz.muni.fi.pv217.household.expense.payment.dtos.PaymentDto;
import cz.muni.fi.pv217.household.expense.payment.models.Payment;
import org.mapstruct.Mapper;

/**
 * Class for mapping RecurringTransaction.
 */
@Mapper(componentModel = "jakarta")
public interface PaymentMapper extends BaseMapper<PaymentDto, Payment> {
}
